(function() {

    /***********
    MODULE: home
    ************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('application/home', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/home","dependencies":{"controls":["paper-toolbar","paper-icon-button","paper-spinner","paper-tabs","iron-pages","iron-image","paper-spinner","paper-scroll-header-panel","paper-badge","graphs-channel","graphs-icons","graphs-conversations","myddoc-icons","myddoc-specialties-list","gyneconet-icons"],"require":{"libraries/graphs/main/code":"graphs","application/home/page":"Page"}}});
        
        
    })(beyond.modules.get('application/home'));
    
    /****************************
    MODULE: home/settings/control
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('application/home/settings/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"gyneconet-settings": "application/home/settings/control"});
        module.control.id = 'gyneconet-settings';
        
        
    })(beyond.modules.get('application/home/settings/control'));
    
    /*************************
    MODULE: home/settings/page
    **************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('application/home/settings/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/settings","dependencies":{"controls":["paper-button","paper-toolbar","iron-icons","graphs-icons","gyneconet-settings","paper-spinner"],"require":{"application/home/settings/page/page":"Page"}}});
        
        
    })(beyond.modules.get('application/home/settings/page'));
    
    /************
    MODULE: icons
    *************/
    
    (function (module) {
    
        module = module[0];
    
        /*************
        icons compiler
        **************/
        
        beyond.controls.register({"gyneconet-icons": {"path":"application/icons","type":"icons"}});
        module.control.id = 'gyneconet-icons';
        
        
    })(beyond.modules.get('application/icons'));
    
    /**************
    MODULE: routing
    ***************/
    
    (function (module) {
    
        module = module[0];
    
        /************
        code compiler
        *************/
        
        /*********
        routing.js
        *********/
        
        beyond.bind('routing', function (pathname, done) {
            "use strict";
        
            if (pathname !== '/') {
                done();
                return;
            }
        
        
            var auth;
        
            function loadUser(auth, graphs) {
        
                auth.user.load()
                    .then(function () {
        
                        if (auth.user.doctor.id) {
                            done({'pathname': '/home'});
                            return;
                        }
        
                        var inbox = graphs.inbox;
        
                        function loadConversationsCount() {
        
                            inbox.updateTotalCounter()
                                .then(function (value) {
                                    if (value) {
                                        done({'pathname': '/home'});
                                        return;
                                    }
                                    else {
                                        done({'pathname': '/home'});
                                        return;
                                    }
                                })
                                .catch(function () {
                                    beyond.showConnectionError(loadConversationsCount);
                                });
        
                        }
        
                        loadConversationsCount();
        
                    });
            }
        
            function navigate(auth, graphs) {
                if (beyond.pages.routes['/welcome'] && !localStorage.getItem('welcome')) {
                    done({'pathname': '/welcome'});
                }
                else if (!auth.valid) {
                    done({'pathname': '/login/main'});
                    return;
                }
                else {
                    done({'pathname': '/home'});
                    loadUser(auth, graphs);
        
                }
            }
        
            var graphs = 'libraries/graphs/main/code/' + beyond.params.language;
            require([graphs], function (graphs) {
        
                var version = _g.graphs.getConfigKey('application', 'version');
                var versionPromise = version.load().promise;
                auth = graphs.auth;
        
                if (!window.versionChecked && beyond.phonegap.isPhonegap) {
                    // Version was not already checked
                    versionPromise.then(function (response) {
        
                        if (response.length && (beyond.params.version < response[0].value.current)) {
        
                            if (beyond.params.version < response[0].value.minCompatibleVersion) {
                                window.versionUncompatible = true;
                            }
                            done({'pathname': '/check_version'});
                            return;
        
                        }
        
                        window.versionChecked = true;
                        navigate(auth, graphs);
        
        
                    });
        
                }
                else {
                    navigate(auth, graphs);
        
                }
        
        
            });
        
        });
        
        
        /*************
        back-button.js
        *************/
        
        function onBackButton() {
        
            if (['/login/main', '/welcome', '/home'].indexOf(beyond.pathname) !== -1) {
                beyond.analytics.exit(function () {
                    navigator.app.exitApp();
                });
                return;
            }
        
            history.back();
        
        }
        
        beyond.phonegap.done(function () {
            document.addEventListener('backbutton', onBackButton, false);
        });
        
        
        
        
        
    })(beyond.modules.get('application/routing'));
    
    /*************
    MODULE: styles
    **************/
    
    (function (module) {
    
        module = module[0];
    
        /************
        code compiler
        *************/
        
        /**********
         CSS STYLES
         **********/
        
        (function() {
        	var styles = '.page-one-control{position:absolute;top:0;right:0;bottom:0;left:0;background-color:#fff;opacity:0;transform:scale(.9) translate3d(0,0,0);transition:transform .4s ease,opacity .4s ease}.page-one-control.show{opacity:1;transform:none}.page-one-control paper-toolbar .title{font-size:16px;margin-left:0}.page-one-control>.control{position:absolute;top:64px;left:0;right:0;bottom:0;overflow-y:auto}@media (max-width:600px){.page-one-control>.control{top:56px}}';
        	var is = '';
        	module.styles.push(styles, is);
        })();
        
        
        
        
    })(beyond.modules.get('application/styles'));
    
    /**************
    MODULE: welcome
    ***************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('application/welcome', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/welcome","dependencies":{"polymer":["paper-button","font-roboto"],"require":{"application/welcome/page":"Page"}}});
        
        
    })(beyond.modules.get('application/welcome'));
    
    /***************
    LIBRARY NAME: ui
    MODULE: toast
    ****************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/ui/toast', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/ui/toast'));
    
    
    
    /*******************
    LIBRARY NAME: myddoc
    MODULE: directory
    ********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/directory', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/myddoc/directory'));
    
    /*******************
    LIBRARY NAME: myddoc
    MODULE: ui/content
    ********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/content', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/content/view","dependencies":{"controls":["myddoc-article-view","graphs-comments-send","graphs-comments-list"],"require":{"libraries/graphs-ui/ui/entities/view/page/code":"PageBase","libraries/myddoc/ui/content/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"myddoc-article-view": "libraries/myddoc/ui/content"});
        module.control.id = 'myddoc-article-view';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/content'));
    
    /*******************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/admin/control
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/admin/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"doctors-admin": "libraries/myddoc/ui/doctors/admin/control"});
        module.control.id = 'doctors-admin';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/admin/control'));
    
    /****************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/admin/page
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/admin/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/doctors/admin","dependencies":{"controls":["paper-toolbar","paper-icon-button","graphs-icons","paper-spinner","doctors-admin"],"require":{"libraries/myddoc/ui/doctors/admin/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/admin/page'));
    
    /***************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/directory
    ****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/directory', ["code","page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/doctors/directory","dependencies":{"require":{"libraries/myddoc/doctors/directory/code":"directory","libraries/myddoc/ui/doctors/directory/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/directory'));
    
    /******************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/list/control
    *******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/list/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"myddoc-doctors-list": "libraries/myddoc/ui/doctors/list/control"});
        module.control.id = 'myddoc-doctors-list';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/list/control'));
    
    /***************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/list/page
    ****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/list/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/doctors/list","dependencies":{"controls":["paper-toolbar","paper-spinner","paper-icon-button","iron-icons","myddoc-doctors-list"],"require":{"libraries/graphs/main/code":"graphs","libraries/myddoc/ui/doctors/list/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/list/page'));
    
    /***************************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/costs/control
    ****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/costs/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"doctor-costs-admin": "libraries/myddoc/ui/doctors/profile/costs/control"});
        module.control.id = 'doctor-costs-admin';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/costs/control'));
    
    /************************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/costs/page
    *************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/costs/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/doctor/costs","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","doctor-costs-admin"],"require":{"libraries/myddoc/ui/doctors/profile/costs/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/costs/page'));
    
    /**************************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/edit/control
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/edit/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"myddoc-doctor-profile": "libraries/myddoc/ui/doctors/profile/edit/control"});
        module.control.id = 'myddoc-doctor-profile';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/edit/control'));
    
    /*****************************************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/edit/my-specialties/control
    ******************************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/edit/my-specialties/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"myddoc-specialty-select": "libraries/myddoc/ui/doctors/profile/edit/my-specialties/control"});
        module.control.id = 'myddoc-specialty-select';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/edit/my-specialties/control'));
    
    /**************************************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/edit/my-specialties/page
    ***************************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/edit/my-specialties/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/my-specialties","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-icons","graphs-icons","myddoc-specialty-select"],"require":{"libraries/myddoc/ui/doctors/profile/edit/my-specialties/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/edit/my-specialties/page'));
    
    /***********************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/edit/page
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/edit/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/profile/doctor","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","myddoc-doctor-profile"],"require":{"libraries/myddoc/ui/doctors/profile/edit/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/edit/page'));
    
    /*************************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/mercadopago
    **************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/mercadopago', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/doctor/mp/token","dependencies":{"controls":["paper-toolbar","paper-button","paper-spinner"],"require":{"libraries/graphs/main/code":"graphs","libraries/myddoc/ui/doctors/profile/mercadopago/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/mercadopago'));
    
    /********************************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/view/communication
    *********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/view/communication', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"doctor-communication": "libraries/myddoc/ui/doctors/profile/view/communication"});
        module.control.id = 'doctor-communication';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/view/communication'));
    
    /***********************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/view/info
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/view/info', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"doctor-info": "libraries/myddoc/ui/doctors/profile/view/info"});
        module.control.id = 'doctor-info';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/view/info'));
    
    /***********************************
    LIBRARY NAME: myddoc
    MODULE: ui/doctors/profile/view/main
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/doctors/profile/view/main', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"doctor-profile-view": "libraries/myddoc/ui/doctors/profile/view/main"});
        module.control.id = 'doctor-profile-view';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/doctors/profile/view/main'));
    
    /*******************
    LIBRARY NAME: myddoc
    MODULE: ui/icons
    ********************/
    
    (function (module) {
    
        module = module[0];
    
        /*************
        icons compiler
        **************/
        
        beyond.controls.register({"myddoc-icons": {"path":"libraries/myddoc/ui/icons","type":"icons"}});
        module.control.id = 'myddoc-icons';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/icons'));
    
    /*************************************
    LIBRARY NAME: myddoc
    MODULE: ui/mercadopago/methods/control
    **************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/mercadopago/methods/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"myddoc-payment-methods": "libraries/myddoc/ui/mercadopago/methods/control"});
        module.control.id = 'myddoc-payment-methods';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/mercadopago/methods/control'));
    
    /**********************************
    LIBRARY NAME: myddoc
    MODULE: ui/mercadopago/methods/page
    ***********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/mercadopago/methods/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/payments/methods","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","myddoc-payment-methods"],"require":{"libraries/myddoc/ui/mercadopago/methods/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/mercadopago/methods/page'));
    
    /*********************************
    LIBRARY NAME: myddoc
    MODULE: ui/mercadopago/pay/control
    **********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/mercadopago/pay/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"mercadopago-pay": "libraries/myddoc/ui/mercadopago/pay/control"});
        module.control.id = 'mercadopago-pay';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/mercadopago/pay/control'));
    
    /******************************
    LIBRARY NAME: myddoc
    MODULE: ui/mercadopago/pay/page
    *******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/mercadopago/pay/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/mercadopago/pay","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","mercadopago-pay"],"require":{"libraries/myddoc/ui/mercadopago/pay/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/mercadopago/pay/page'));
    
    /***********************************
    LIBRARY NAME: myddoc
    MODULE: ui/specialties/admin/control
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/specialties/admin/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"specialties-admin": "libraries/myddoc/ui/specialties/admin/control"});
        module.control.id = 'specialties-admin';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/specialties/admin/control'));
    
    /********************************
    LIBRARY NAME: myddoc
    MODULE: ui/specialties/admin/page
    *********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/specialties/admin/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/specialties/admin","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","specialties-admin"],"require":{"libraries/myddoc/ui/specialties/admin/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/specialties/admin/page'));
    
    /****************************
    LIBRARY NAME: myddoc
    MODULE: ui/specialties/delete
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/specialties/delete', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/specialty/delete","dependencies":{"controls":["graphs-especialty-delete"],"require":{"libraries/graphs/ui/entities/delete/page/code":"PageBase","libraries/myddoc/ui/specialties/delete/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-especialty-delete": "libraries/myddoc/ui/specialties/delete"});
        module.control.id = 'graphs-especialty-delete';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/specialties/delete'));
    
    /**********************************
    LIBRARY NAME: myddoc
    MODULE: ui/specialties/list/control
    ***********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/specialties/list/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"myddoc-specialties-list": "libraries/myddoc/ui/specialties/list/control"});
        module.control.id = 'myddoc-specialties-list';
        
        
    })(beyond.modules.get('libraries/myddoc/ui/specialties/list/control'));
    
    /*******************************
    LIBRARY NAME: myddoc
    MODULE: ui/specialties/list/page
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/specialties/list/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/specialties/list","dependencies":{"controls":["paper-toolbar","paper-spinner","paper-icon-button","iron-icons","myddoc-specialties-list"],"require":{"libraries/myddoc/ui/specialties/list/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/specialties/list/page'));
    
    /*********************
    LIBRARY NAME: myddoc
    MODULE: ui/web-warning
    **********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/myddoc/ui/web-warning', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/web_warning/new","dependencies":{"controls":["paper-button","paper-icon-button","font-roboto","paper-toast","iron-icon","iron-icons","graphs-icons"],"require":{"libraries/graphs/main/code":"graphs","libraries/myddoc/ui/web-warning/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/myddoc/ui/web-warning'));
    
    
    
    /*******************
    LIBRARY NAME: graphs
    MODULE: main
    ********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs/main', ["code"]);
        
        
        /************
        code compiler
        *************/
        
        /********
        socket.js
        ********/
        
        beyond.sockets.bind('connect:before', function (query) {
            "use strict";
        
            var application = beyond.params.application;
            if (application) query.application = JSON.stringify(application);
        
        });
        
        
        /******
        urls.js
        ******/
        
        function Urls() {
            "use strict";
        
            this.get = function (pathname) {
        
                var action = new module.Action('urls/get', {'pathname': pathname});
                return action.execute({'promise': true, 'policy': action.POLICY.COMMUNICATION_ERRORS});
        
            };
        
        }
        
        window.urls = new Urls();
        
        
        /*********
        routing.js
        *********/
        
        beyond.bind('routing', function (pathname, done) {
            "use strict";
        
            if (pathname === '/') {
                done();
                return;
            }
        
            var params = beyond.params.graphs;
            if (typeof params !== 'object' || typeof params.routes !== 'object') {
                console.warn('Application should define the parameter graphs.routes');
                done();
                return;
            }
        
            var routes = params.routes;
        
            beyond.removeMessage('routing');
            var urls = window.urls;
        
            function navigate() {
        
                urls.get(pathname).promise
                    .then(function (graph) {
        
                        if (!graph) {
                            console.log('Invalid pathname "' + pathname + '"');
                            return;
                        }
        
                        var moduleId = routes[graph.entity.id];
        
                        if (!moduleId) {
                            console.warn('Route for entity "' + graph.entity.id + '" is not defined');
                            moduleId = routes.error
                        }
                        if (!moduleId) {
                            moduleId = 'libraries/graphs/pages/error';
                        }
        
                        done({
                            'moduleID': moduleId,
                            'state': graph
                        });
        
                    })
                    .catch(function (error) {
        
                        console.log('Page "' + pathname + '" error', error);
        
                        if (error.code === 4) {
                            beyond.showMessage('Instancia no encontrada.');
                            beyond.navigate('/');
                            return;
                        }
        
                        beyond.showMessage({
                            'type': beyond.toast.MESSAGE_TYPE.CONNECTION_ERROR,
                            'id': 'routing',
                            'retry': navigate
                        });
        
                    });
        
            }
        
            navigate();
        
        });
        
        
        /******
        push.js
        ******/
        
        require([module.ID + '/code/' + beyond.params.language], function (graphs) {
            "use strict";
        
            var push = beyond.phonegap.push;
            var auth = graphs.auth;
        
            function register(registrationId) {
                graphs.push.register(registrationId).promise
                    .catch(function (error) {
                        console.error('Error registering for push notifications', error);
                        beyond.showWarning('Error registrando dispositivo');
                    });
            }
        
            auth.bind('change', function () {
                if (auth.valid && push.registrationId) {
                    register(push.registrationId);
                }
            });
        
            if (push.registrationId) {
                register(push.registrationId);
            }
            else {
                push.bind('registration', function () {
                    if (auth.valid) {
                        register(push.registrationId);
                    }
                });
            }
        
        });
        
        
        /****************
        require.config.js
        ****************/
        
        require.config({
            shim: {
                'facebook': {exports: 'FB'},
                'twitter': {exports: 'twttr'}
            },
            paths: {
                'facebook': 'https://connect.facebook.com/en_US/sdk',
                'twitter': 'https://platform.twitter.com/widgets'
            }
        });
        
        
        
        
        
    })(beyond.modules.get('libraries/graphs/main'));
    
    
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: address/select
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/address/select', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/address/select","dependencies":{"controls":["paper-button","paper-toolbar"],"require":{"libraries/graphs-ui/address/select/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/address/select'));
    
    /***********************
    LIBRARY NAME: graphs-ui
    MODULE: auth/login/email
    ************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/login/email', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/login/email","dependencies":{"controls":["paper-material","paper-button","paper-toolbar","paper-menu","paper-icon-button","paper-item","iron-iconset","paper-input","graphs-icons","iron-icons","iron-form","gold-email-input","paper-toast","paper-spinner"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/auth/login/email/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/login/email'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: auth/login/main
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/login/main', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/login/main","dependencies":{"controls":["paper-button","font-roboto","paper-spinner","paper-toast","iron-icon","iron-icons","graphs-icons"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/auth/login/main/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/login/main'));
    
    /**************************
    LIBRARY NAME: graphs-ui
    MODULE: auth/logout/control
    ***************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/logout/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-logout": "libraries/graphs-ui/auth/logout/control"});
        module.control.id = 'graphs-logout';
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/logout/control'));
    
    /***********************
    LIBRARY NAME: graphs-ui
    MODULE: auth/logout/page
    ************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/logout/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/logout","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-icons","graphs-logout"],"require":{"libraries/graphs-ui/auth/logout/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/logout/page'));
    
    /***************************************
    LIBRARY NAME: graphs-ui
    MODULE: auth/recover-password/send-token
    ****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/recover-password/send-token', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/recover_password/send_token","dependencies":{"controls":["paper-toolbar","paper-icon-button","paper-item","iron-iconset","iron-icons","paper-material","iron-form","paper-input","paper-button","gold-email-input","paper-spinner","paper-dialog","neon-animations"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/auth/recover-password/send-token/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/recover-password/send-token'));
    
    /*****************************************
    LIBRARY NAME: graphs-ui
    MODULE: auth/recover-password/set-password
    ******************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/recover-password/set-password', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/set_password","dependencies":{"controls":["paper-toolbar","paper-icon-button","paper-item","iron-iconset","iron-icons","graphs-icons","iron-form","paper-input","paper-button","paper-spinner","paper-dialog","neon-animations"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/auth/recover-password/set-password/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/recover-password/set-password'));
    
    /**********************************
    LIBRARY NAME: graphs-ui
    MODULE: auth/register/register-user
    ***********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/register/register-user', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/register_user","dependencies":{"controls":["paper-toolbar","paper-icon-button","paper-item","iron-iconset","iron-icons","iron-form","graphs-icons","paper-input","paper-button","paper-spinner","paper-dialog","neon-animations"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/auth/register/register-user/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/register/register-user'));
    
    /*******************************
    LIBRARY NAME: graphs-ui
    MODULE: auth/register/send-token
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/auth/register/send-token', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/register_user/send_token","dependencies":{"controls":["paper-toolbar","paper-icon-button","paper-item","iron-iconset","iron-icons","paper-material","iron-form","paper-input","paper-button","gold-email-input","paper-spinner","paper-dialog","neon-animations"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/auth/register/send-token/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/auth/register/send-token'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: check-version
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/check-version', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/check_version","dependencies":{"controls":["paper-button","paper-icon-button","font-roboto","paper-toast","iron-icon","iron-icons","graphs-icons"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/check-version/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/check-version'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: controls
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/controls', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/controls","dependencies":{"controls":["iron-icon","iron-pages","iron-image","paper-button","paper-toolbar","paper-textarea","paper-input","paper-checkbox","paper-radio-button","paper-radio-group","paper-tabs","paper-spinner","paper-scroll-header-panel","paper-badge","paper-icon-button","graphs-icons"],"require":{"libraries/graphs-ui/controls/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/controls'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/brands/admin
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/brands/admin', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/brands/admin","dependencies":{"controls":["graphs-brands-admin"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/brands/admin/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-brands-admin": "libraries/graphs-ui/entities/brands/admin"});
        module.control.id = 'graphs-brands-admin';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/brands/admin'));
    
    /*****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/brands/delete
    ******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/brands/delete', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/brand/delete","dependencies":{"controls":["graphs-brand-delete"],"require":{"libraries/graphs-ui/ui/entities/delete/page/code":"PageBase","libraries/graphs-ui/entities/brands/delete/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-brand-delete": "libraries/graphs-ui/entities/brands/delete"});
        module.control.id = 'graphs-brand-delete';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/brands/delete'));
    
    /***************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/brands/edit
    ****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/brands/edit', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/brand/edit","dependencies":{"controls":["graphs-brand-edit"],"require":{"libraries/graphs-ui/ui/entities/edit/page/code":"PageBase","libraries/graphs-ui/entities/brands/edit/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-brand-edit": "libraries/graphs-ui/entities/brands/edit"});
        module.control.id = 'graphs-brand-edit';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/brands/edit'));
    
    /***************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/brands/list
    ****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/brands/list', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/brands/list","dependencies":{"controls":["graphs-brands-list"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/brands/list/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-brands-list": "libraries/graphs-ui/entities/brands/list"});
        module.control.id = 'graphs-brands-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/brands/list'));
    
    /*******************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/brands/select-brand/control
    ********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/brands/select-brand/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-select-brand": "libraries/graphs-ui/entities/brands/select-brand/control"});
        module.control.id = 'graphs-select-brand';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/brands/select-brand/control'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/brands/share
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/brands/share', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/brand/share","dependencies":{"controls":["graphs-brand-share"],"require":{"libraries/graphs-ui/ui/entities/share/page/code":"PageBase","libraries/graphs-ui/entities/brands/share/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-brand-share": "libraries/graphs-ui/entities/brands/share"});
        module.control.id = 'graphs-brand-share';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/brands/share'));
    
    /***************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/brands/view
    ****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/brands/view', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/brand/view","dependencies":{"controls":["graphs-brand-view"],"require":{"libraries/graphs-ui/ui/entities/view/page/code":"PageBase","libraries/graphs-ui/entities/brands/view/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-brand-view": "libraries/graphs-ui/entities/brands/view"});
        module.control.id = 'graphs-brand-view';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/brands/view'));
    
    /**************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/comments/controls/list
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/comments/controls/list', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-comments-list": "libraries/graphs-ui/entities/comments/controls/list"});
        module.control.id = 'graphs-comments-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/comments/controls/list'));
    
    /**************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/comments/controls/send
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/comments/controls/send', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-comments-send": "libraries/graphs-ui/entities/comments/controls/send"});
        module.control.id = 'graphs-comments-send';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/comments/controls/send'));
    
    /*****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/comments/page
    ******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/comments/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/comments","dependencies":{"controls":["paper-toolbar","paper-icon-button","paper-input","paper-textarea","paper-spinner","iron-icons","graphs-icons","graphs-comments-list","graphs-comments-send"],"require":{"libraries/graphs-ui/entities/comments/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/comments/page'));
    
    /********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/communities/edit
    *********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/communities/edit', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/community/edit","dependencies":{"controls":["graphs-community-edit"],"require":{"libraries/graphs-ui/ui/entities/edit/page/code":"PageBase","libraries/graphs-ui/entities/communities/edit/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-community-edit": "libraries/graphs-ui/entities/communities/edit"});
        module.control.id = 'graphs-community-edit';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/communities/edit'));
    
    /****************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/admin/all-posts
    *****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/admin/all-posts', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/posts/all/admin","dependencies":{"controls":["graphs-posts-all-admin"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/contents/admin/all-posts/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-posts-all-admin": "libraries/graphs-ui/entities/contents/admin/all-posts"});
        module.control.id = 'graphs-posts-all-admin';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/admin/all-posts'));
    
    /***************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/admin/my-posts
    ****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/admin/my-posts', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/posts/admin","dependencies":{"controls":["graphs-posts-admin"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/contents/admin/my-posts/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-posts-admin": "libraries/graphs-ui/entities/contents/admin/my-posts"});
        module.control.id = 'graphs-posts-admin';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/admin/my-posts'));
    
    /**************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/articles/edit
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/articles/edit', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/article/edit","dependencies":{"controls":["graphs-article-edit"],"require":{"libraries/graphs-ui/ui/entities/edit/page/code":"PageBase","libraries/graphs-ui/entities/contents/articles/edit/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-article-edit": "libraries/graphs-ui/entities/contents/articles/edit"});
        module.control.id = 'graphs-article-edit';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/articles/edit'));
    
    /**********************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/articles/list/control
    ***********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/articles/list/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-articles-list": "libraries/graphs-ui/entities/contents/articles/list/control"});
        module.control.id = 'graphs-articles-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/articles/list/control'));
    
    /*******************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/articles/list/page
    ********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/articles/list/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/articles/list","dependencies":{"controls":["paper-toolbar","paper-spinner","paper-icon-button","iron-icons","graphs-articles-list"],"require":{"libraries/graphs-ui/entities/contents/articles/list/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/articles/list/page'));
    
    /*****************************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/channel/entry-delete/control
    ******************************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/channel/entry-delete/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-content-delete": "libraries/graphs-ui/entities/contents/channel/entry-delete/control"});
        module.control.id = 'graphs-content-delete';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/channel/entry-delete/control'));
    
    /**************************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/channel/entry-delete/page
    ***************************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/channel/entry-delete/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/contents/delete","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-icons","paper-spinner","graphs-content-delete"],"require":{"libraries/graphs-ui/entities/contents/channel/entry-delete/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/channel/entry-delete/page'));
    
    /*********************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/channel/view/control
    **********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/channel/view/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-channel": "libraries/graphs-ui/entities/contents/channel/view/control"});
        module.control.id = 'graphs-channel';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/channel/view/control'));
    
    /******************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/channel/view/page
    *******************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/channel/view/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/channel/view","dependencies":{"controls":["paper-toolbar","paper-spinner","paper-icon-button","iron-icons","graphs-channel"],"require":{"libraries/graphs-ui/entities/contents/channel/view/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/channel/view/page'));
    
    /*******************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/delete
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/delete', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/content/delete","dependencies":{"controls":["graphs-post-delete"],"require":{"libraries/graphs-ui/ui/entities/delete/page/code":"PageBase","libraries/graphs-ui/entities/contents/delete/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-post-delete": "libraries/graphs-ui/entities/contents/delete"});
        module.control.id = 'graphs-post-delete';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/delete'));
    
    /************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/links/admin
    *************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/links/admin', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/links/admin","dependencies":{"controls":["graphs-links-admin"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/contents/links/admin/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-links-admin": "libraries/graphs-ui/entities/contents/links/admin"});
        module.control.id = 'graphs-links-admin';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/links/admin'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/links/edit
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/links/edit', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/links/edit","dependencies":{"controls":["graphs-links-edit"],"require":{"libraries/graphs-ui/ui/entities/edit/page/code":"PageBase","libraries/graphs-ui/entities/contents/links/edit/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-links-edit": "libraries/graphs-ui/entities/contents/links/edit"});
        module.control.id = 'graphs-links-edit';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/links/edit'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/links/list
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/links/list', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/links/list","dependencies":{"controls":["graphs-links-list"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/contents/links/list/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-links-list": "libraries/graphs-ui/entities/contents/links/list"});
        module.control.id = 'graphs-links-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/links/list'));
    
    /**********************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/posts/publish/control
    ***********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/posts/publish/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-post-publish": "libraries/graphs-ui/entities/contents/posts/publish/control"});
        module.control.id = 'graphs-post-publish';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/posts/publish/control'));
    
    /*******************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/posts/publish/page
    ********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/posts/publish/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/post/publish","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-post-publish"],"require":{"libraries/graphs-ui/entities/contents/posts/publish/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/posts/publish/page'));
    
    /***********************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/send-to-screen/control
    ************************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/send-to-screen/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-content-send-to-screen": "libraries/graphs-ui/entities/contents/send-to-screen/control"});
        module.control.id = 'graphs-content-send-to-screen';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/send-to-screen/control'));
    
    /********************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/send-to-screen/page
    *********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/send-to-screen/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/content/sendToScreen","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-icons","graphs-content-send-to-screen"],"require":{"libraries/graphs-ui/entities/contents/send-to-screen/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/send-to-screen/page'));
    
    /**************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/share/control
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/share/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-content-share": "libraries/graphs-ui/entities/contents/share/control"});
        module.control.id = 'graphs-content-share';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/share/control'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/share/page
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/share/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/content/share","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-icons","graphs-content-share","paper-spinner"],"require":{"libraries/graphs-ui/entities/contents/share/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/share/page'));
    
    /*****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/contents/view
    ******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/contents/view', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/article/view","dependencies":{"controls":["graphs-article-view","graphs-comments-send","graphs-comments-list"],"require":{"libraries/graphs-ui/ui/entities/view/page/code":"PageBase","libraries/graphs-ui/entities/contents/view/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-article-view": "libraries/graphs-ui/entities/contents/view"});
        module.control.id = 'graphs-article-view';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/contents/view'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/admin
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/admin', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/photos/admin","dependencies":{"controls":["graphs-photos-admin"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/photos/admin/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photos-admin": "libraries/graphs-ui/entities/photos/admin"});
        module.control.id = 'graphs-photos-admin';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/admin'));
    
    /*******************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/attach/graph/control
    ********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/attach/graph/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-attach-graph": "libraries/graphs-ui/entities/photos/attach/graph/control"});
        module.control.id = 'graphs-attach-graph';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/attach/graph/control'));
    
    /****************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/attach/graph/page
    *****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/attach/graph/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/attach/graph","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-attach-graph"],"require":{"libraries/graphs-ui/entities/photos/attach/graph/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/attach/graph/page'));
    
    /**************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/attach/list.old
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/attach/list.old', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/attach/photos/list","dependencies":{"controls":["graphs-photos-attach"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/photos/attach/list.old/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photos-attach": "libraries/graphs-ui/entities/photos/attach/list.old"});
        module.control.id = 'graphs-photos-attach';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/attach/list.old'));
    
    /********************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/attach/upload/control
    *********************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/attach/upload/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photo-attach": "libraries/graphs-ui/entities/photos/attach/upload/control"});
        module.control.id = 'graphs-photo-attach';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/attach/upload/control'));
    
    /*****************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/attach/upload/page
    ******************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/attach/upload/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/photo/attach","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-photo-attach"],"require":{"libraries/graphs-ui/entities/photos/attach/upload/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/attach/upload/page'));
    
    /*****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/delete
    ******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/delete', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/photo/delete","dependencies":{"controls":["graphs-photo-delete"],"require":{"libraries/graphs-ui/ui/entities/delete/page/code":"PageBase","libraries/graphs-ui/entities/photos/delete/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photo-delete": "libraries/graphs-ui/entities/photos/delete"});
        module.control.id = 'graphs-photo-delete';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/delete'));
    
    /***************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/edit
    ****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/edit', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/photo/edit","dependencies":{"controls":["graphs-photo-edit"],"require":{"libraries/graphs-ui/ui/entities/edit/page/code":"PageBase","libraries/graphs-ui/entities/photos/edit/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photo-edit": "libraries/graphs-ui/entities/photos/edit"});
        module.control.id = 'graphs-photo-edit';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/edit'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/list/control
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/list/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photos-list": "libraries/graphs-ui/entities/photos/list/control"});
        module.control.id = 'graphs-photos-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/list/control'));
    
    /********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/list/page
    *********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/list/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/photos/list","dependencies":{"controls":["paper-button","paper-toolbar","iron-icons","graphs-icons","paper-spinner","graphs-photos-list"],"require":{"libraries/graphs-ui/entities/photos/list/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/list/page'));
    
    /*************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/upload/control
    **************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/upload/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photo-upload": "libraries/graphs-ui/entities/photos/upload/control"});
        module.control.id = 'graphs-photo-upload';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/upload/control'));
    
    /**********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/upload/page
    ***********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/upload/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/photo/upload","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-photo-upload"],"require":{"libraries/graphs-ui/entities/photos/upload/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/upload/page'));
    
    /***************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/photos/view
    ****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/photos/view', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/photo/view","dependencies":{"controls":["graphs-photo-view"],"require":{"libraries/graphs-ui/ui/entities/view/page/code":"PageBase","libraries/graphs-ui/entities/photos/view/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-photo-view": "libraries/graphs-ui/entities/photos/view"});
        module.control.id = 'graphs-photo-view';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/photos/view'));
    
    /*****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/service/admin
    ******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/service/admin', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/service/admin","dependencies":{"controls":["graphs-service-admin"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/service/admin/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-service-admin": "libraries/graphs-ui/entities/service/admin"});
        module.control.id = 'graphs-service-admin';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/service/admin'));
    
    /******************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/service/delete
    *******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/service/delete', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/service/delete","dependencies":{"controls":["graphs-service-delete"],"require":{"libraries/graphs-ui/ui/entities/delete/page/code":"PageBase","libraries/graphs-ui/entities/service/delete/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-service-delete": "libraries/graphs-ui/entities/service/delete"});
        module.control.id = 'graphs-service-delete';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/service/delete'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/service/edit
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/service/edit', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/service/edit","dependencies":{"controls":["graphs-service-edit"],"require":{"libraries/graphs-ui/ui/entities/edit/page/code":"PageBase","libraries/graphs-ui/entities/service/edit/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-service-edit": "libraries/graphs-ui/entities/service/edit"});
        module.control.id = 'graphs-service-edit';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/service/edit'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/service/list
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/service/list', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/service/list","dependencies":{"controls":["graphs-service-list"],"require":{"libraries/graphs-ui/ui/entities/list/page/code":"PageBase","libraries/graphs-ui/entities/service/list/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-service-list": "libraries/graphs-ui/entities/service/list"});
        module.control.id = 'graphs-service-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/service/list'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/admin/control
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/admin/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-users-admin": "libraries/graphs-ui/entities/users/admin/control"});
        module.control.id = 'graphs-users-admin';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/admin/control'));
    
    /********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/admin/page
    *********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/admin/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/users/admin","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-users-admin"],"require":{"libraries/graphs-ui/entities/users/admin/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/admin/page'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/block/control
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/block/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-users-block": "libraries/graphs-ui/entities/users/block/control"});
        module.control.id = 'graphs-users-block';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/block/control'));
    
    /********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/block/page
    *********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/block/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/users/block","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-users-block"],"require":{"libraries/graphs-ui/entities/users/block/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/block/page'));
    
    /***************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/followers/control
    ****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/followers/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-users-followers": "libraries/graphs-ui/entities/users/followers/control"});
        module.control.id = 'graphs-users-followers';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/followers/control'));
    
    /************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/followers/page
    *************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/followers/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/users/list","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-users-followers"],"require":{"libraries/graphs-ui/entities/users/followers/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/followers/page'));
    
    /**********************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/list/control
    ***********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/list/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-users-list": "libraries/graphs-ui/entities/users/list/control"});
        module.control.id = 'graphs-users-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/list/control'));
    
    /*******************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/list/page
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/list/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/users/list","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-users-list"],"require":{"libraries/graphs-ui/entities/users/list/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/list/page'));
    
    /******************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/profile/edit/control
    *******************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/profile/edit/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-profile": "libraries/graphs-ui/entities/users/profile/edit/control"});
        module.control.id = 'graphs-profile';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/profile/edit/control'));
    
    /***************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/profile/edit/page
    ****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/profile/edit/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/profile","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-profile"],"require":{"libraries/graphs-ui/entities/users/profile/edit/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/profile/edit/page'));
    
    /*************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/profile/routing
    **************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/profile/routing', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"dependencies":{"controls":["paper-icon-button","iron-icons","graphs-icons","paper-toolbar","paper-spinner"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/entities/users/profile/routing/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/profile/routing'));
    
    /***************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/profile/view/info
    ****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/profile/view/info', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"user-info": "libraries/graphs-ui/entities/users/profile/view/info"});
        module.control.id = 'user-info';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/profile/view/info'));
    
    /***************************************
    LIBRARY NAME: graphs-ui
    MODULE: entities/users/profile/view/main
    ****************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/entities/users/profile/view/main', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-profile-view": "libraries/graphs-ui/entities/users/profile/view/main"});
        module.control.id = 'graphs-profile-view';
        
        
    })(beyond.modules.get('libraries/graphs-ui/entities/users/profile/view/main'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: error
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/error', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"dependencies":{"controls":["paper-button"],"require":{"libraries/graphs-ui/error/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/error'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: feedback/action
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/feedback/action', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-feedback-action": "libraries/graphs-ui/feedback/action"});
        module.control.id = 'graphs-feedback-action';
        
        
    })(beyond.modules.get('libraries/graphs-ui/feedback/action'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: feedback/list/control
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/feedback/list/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-feedback-list": "libraries/graphs-ui/feedback/list/control"});
        module.control.id = 'graphs-feedback-list';
        
        
    })(beyond.modules.get('libraries/graphs-ui/feedback/list/control'));
    
    /*************************
    LIBRARY NAME: graphs-ui
    MODULE: feedback/list/page
    **************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/feedback/list/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/feedback/list","dependencies":{"controls":["paper-button","paper-toolbar","iron-icons","graphs-icons","paper-spinner","graphs-feedback-list"],"require":{"libraries/graphs-ui/feedback/list/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/feedback/list/page'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: feedback/view/control
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/feedback/view/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-feedback-view": "libraries/graphs-ui/feedback/view/control"});
        module.control.id = 'graphs-feedback-view';
        
        
    })(beyond.modules.get('libraries/graphs-ui/feedback/view/control'));
    
    /*************************
    LIBRARY NAME: graphs-ui
    MODULE: feedback/view/page
    **************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/feedback/view/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/feedback/view","dependencies":{"controls":["paper-button","paper-toolbar","iron-icons","graphs-icons","paper-spinner","graphs-feedback-view"],"require":{"libraries/graphs-ui/feedback/view/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/feedback/view/page'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: icons
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*************
        icons compiler
        **************/
        
        beyond.controls.register({"graphs-icons": {"path":"libraries/graphs-ui/icons","type":"icons"}});
        module.control.id = 'graphs-icons';
        
        
    })(beyond.modules.get('libraries/graphs-ui/icons'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: inbox/compose/control
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/inbox/compose/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"conversation-compose": "libraries/graphs-ui/inbox/compose/control"});
        module.control.id = 'conversation-compose';
        
        
    })(beyond.modules.get('libraries/graphs-ui/inbox/compose/control'));
    
    /*************************
    LIBRARY NAME: graphs-ui
    MODULE: inbox/compose/page
    **************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/inbox/compose/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/conversation/compose","dependencies":{"controls":["paper-toolbar","paper-spinner","paper-icon-button","iron-icons","conversation-compose"],"require":{"libraries/graphs-ui/inbox/compose/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/inbox/compose/page'));
    
    /*********************************
    LIBRARY NAME: graphs-ui
    MODULE: inbox/conversation/control
    **********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/inbox/conversation/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-conversation": "libraries/graphs-ui/inbox/conversation/control"});
        module.control.id = 'graphs-conversation';
        
        
    })(beyond.modules.get('libraries/graphs-ui/inbox/conversation/control'));
    
    /******************************
    LIBRARY NAME: graphs-ui
    MODULE: inbox/conversation/page
    *******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/inbox/conversation/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/conversation/view","dependencies":{"controls":["paper-toolbar","paper-icon-button","paper-spinner","iron-iconset","iron-icons","graphs-icons","graphs-conversation"],"require":{"libraries/graphs-ui/inbox/conversation/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/inbox/conversation/page'));
    
    /**********************************
    LIBRARY NAME: graphs-ui
    MODULE: inbox/conversations/control
    ***********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/inbox/conversations/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-conversations": "libraries/graphs-ui/inbox/conversations/control"});
        module.control.id = 'graphs-conversations';
        
        
    })(beyond.modules.get('libraries/graphs-ui/inbox/conversations/control'));
    
    /*******************************
    LIBRARY NAME: graphs-ui
    MODULE: inbox/conversations/page
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/inbox/conversations/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/conversations","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-iconset","iron-icons","paper-spinner","graphs-conversations"],"require":{"libraries/graphs-ui/inbox/conversations/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/inbox/conversations/page'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: information
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/information', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/information","dependencies":{"controls":["paper-button","paper-icon-button","paper-toolbar","iron-icons","iron-icon","graphs-icons","paper-spinner"],"require":{"libraries/graphs-ui/information/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/information'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: jwvideo
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/jwvideo', ["control"]);
        
        
        /************
        code compiler
        *************/
        
        /*************
        start\start.js
        *************/
        
        (function (config, vendorPath) {
            "use strict";
        
            var paths = {
                'jwplayer': vendorPath + '/jwplayer',
        
                'YT': 'https://www.youtube.com/iframe_api?noext!undefined:onYouTubeIframeAPIReady'
            };
        
            requirejs.config({
                'shim': {
                    'jwplayer': {exports: 'jwplayer'},
                    'YT': {exports: 'YT'}
                },
                paths: paths
            });
        
        })(requirejs.config, beyond.requireConfig.paths['libraries/graphs'] + '/jwvideo/static/vendor');
        
        
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-jwvideo": "libraries/graphs-ui/jwvideo"});
        module.control.id = 'graphs-jwvideo';
        
        
    })(beyond.modules.get('libraries/graphs-ui/jwvideo'));
    
    /**************************
    LIBRARY NAME: graphs-ui
    MODULE: permissions/control
    ***************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/permissions/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-permissions": "libraries/graphs-ui/permissions/control"});
        module.control.id = 'graphs-permissions';
        
        
    })(beyond.modules.get('libraries/graphs-ui/permissions/control'));
    
    /*****************************************
    LIBRARY NAME: graphs-ui
    MODULE: permissions/my-permissions/control
    ******************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/permissions/my-permissions/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-my-permissions": "libraries/graphs-ui/permissions/my-permissions/control"});
        module.control.id = 'graphs-my-permissions';
        
        
    })(beyond.modules.get('libraries/graphs-ui/permissions/my-permissions/control'));
    
    /***********************
    LIBRARY NAME: graphs-ui
    MODULE: permissions/page
    ************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/permissions/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/permissions","dependencies":{"controls":["paper-button","paper-toolbar","paper-input","graphs-permissions","paper-spinner"],"require":{"libraries/graphs-ui/permissions/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/permissions/page'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: picture
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/picture', ["page"]);
        
        
        /************
        code compiler
        *************/
        
        /*************
        start\start.js
        *************/
        
        (function (config, vendorPath) {
            "use strict";
        
            var paths = {
                'hammer': vendorPath + '/hammer.min',
                'mousewheel': vendorPath + '/jquery.mousewheel.min',
                'pinchzoomer': vendorPath + '/jquery.pinchzoomer.min',
                'tweenmax': vendorPath + '/tweenmax.min'
            };
        
            requirejs.config({
                paths: paths
            });
        
        })(requirejs.config, beyond.requireConfig.paths['libraries/graphs-ui'] + '/picture/static/vendor');
        
        
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/picture","dependencies":{"controls":["paper-toolbar","paper-icon-button","iron-icons","graphs-icons","paper-spinner"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/picture/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/picture'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: search/control
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/search/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-search": "libraries/graphs-ui/search/control"});
        module.control.id = 'graphs-search';
        
        
    })(beyond.modules.get('libraries/graphs-ui/search/control'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: search/page
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/search/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/search","dependencies":{"controls":["paper-button","paper-toolbar","paper-input","graphs-search","paper-spinner"],"require":{"libraries/graphs-ui/search/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/search/page'));
    
    /**************************************
    LIBRARY NAME: graphs-ui
    MODULE: social/facebook/fb-page/control
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/social/facebook/fb-page/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-facebook-page": "libraries/graphs-ui/social/facebook/fb-page/control"});
        module.control.id = 'graphs-facebook-page';
        
        
    })(beyond.modules.get('libraries/graphs-ui/social/facebook/fb-page/control'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: social/facebook/fb-page/page
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/social/facebook/fb-page/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"facebook/page","dependencies":{"controls":["paper-button","paper-toolbar","iron-icons","iron-iconset","paper-icon-button","graphs-icons","graphs-facebook-page","paper-spinner"],"require":{"libraries/graphs-ui/social/facebook/fb-page/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/social/facebook/fb-page/page'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: social/spotify/playlist/page
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/social/spotify/playlist/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/spotify/play","dependencies":{"controls":["paper-button","paper-toolbar","iron-icons","iron-iconset","paper-icon-button","graphs-icons"],"require":{"libraries/graphs-ui/social/spotify/playlist/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/social/spotify/playlist/page'));
    
    /**************************************
    LIBRARY NAME: graphs-ui
    MODULE: social/twitter/timeline/control
    ***************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/social/twitter/timeline/control', ["control"]);
        
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-twitter-timeline": "libraries/graphs-ui/social/twitter/timeline/control"});
        module.control.id = 'graphs-twitter-timeline';
        
        
    })(beyond.modules.get('libraries/graphs-ui/social/twitter/timeline/control'));
    
    /***********************************
    LIBRARY NAME: graphs-ui
    MODULE: social/twitter/timeline/page
    ************************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/social/twitter/timeline/page', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"twitter/timeline","dependencies":{"controls":["paper-button","paper-toolbar","iron-icons","iron-iconset","paper-icon-button","graphs-icons","graphs-twitter-timeline"],"require":{"libraries/graphs-ui/social/twitter/timeline/page/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/social/twitter/timeline/page'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: tests
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/tests', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/tests","dependencies":{"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/tests/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/tests'));
    
    /*********************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/delete/control
    **********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/delete/control', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/delete/control'));
    
    /******************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/delete/page
    *******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/delete/page', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/delete/page'));
    
    /*******************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/edit/control
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/edit/control', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/edit/control'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/edit/page
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/edit/page', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/edit/page'));
    
    /*******************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/list/control
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/list/control', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/list/control'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/list/page
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/list/page', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/list/page'));
    
    /********************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/share/control
    *********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/share/control', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/share/control'));
    
    /*****************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/share/page
    ******************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/share/page', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/share/page'));
    
    /*******************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/view/control
    ********************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/view/control', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/view/control'));
    
    /****************************
    LIBRARY NAME: graphs-ui
    MODULE: ui/entities/view/page
    *****************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/ui/entities/view/page', ["code"]);
        
        
        
    })(beyond.modules.get('libraries/graphs-ui/ui/entities/view/page'));
    
    /**************************
    LIBRARY NAME: graphs-ui
    MODULE: video/trash/control
    ***************************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/video/trash/control', ["page","control"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/video","dependencies":{"controls":["graphs-video-view"],"require":{"libraries/graphs-ui/video/trash/control/page":"Page"}}});
        
        /***************
        control compiler
        ****************/
        
        beyond.controls.register({"graphs-video-view": "libraries/graphs-ui/video/trash/control"});
        module.control.id = 'graphs-video-view';
        
        
    })(beyond.modules.get('libraries/graphs-ui/video/trash/control'));
    
    /**********************
    LIBRARY NAME: graphs-ui
    MODULE: web-warning
    ***********************/
    
    (function (module) {
    
        module = module[0];
    
        /*********************
        multilanguage compiler
        **********************/
        
        beyond.modules.multilanguage.set('libraries/graphs-ui/web-warning', ["page"]);
        
        
        /************
        page compiler
        *************/
        
        beyond.pages.register(module, {"route":"/web_warning","dependencies":{"controls":["paper-button","paper-icon-button","font-roboto","paper-toast","iron-icon","iron-icons","graphs-icons"],"require":{"libraries/graphs/main/code":"graphs","libraries/graphs-ui/web-warning/page":"Page"}}});
        
        
    })(beyond.modules.get('libraries/graphs-ui/web-warning'));
    
    
    
    
    beyond.start();

})();