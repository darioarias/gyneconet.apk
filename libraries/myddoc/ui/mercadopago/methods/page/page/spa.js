/**********************************
LIBRARY NAME: myddoc
MODULE: ui/mercadopago/methods/page
***********************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = undefined;

    /**********
     CSS STYLES
     **********/
    
    (function() {
    	var styles = '#payment-methods-page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:#fff;opacity:0;transform:scale(.9) translate3d(0,0,0);transition:transform .4s ease,opacity .4s ease}#payment-methods-page.show{opacity:1;transform:none}#payment-methods-page paper-toolbar .title{font-size:16px;margin-left:0}#payment-methods-page myddoc-payment-methods{position:absolute;top:64px;left:0;right:0;bottom:0;padding:0;overflow-y:auto}@media (max-width:600px){#payment-methods-page myddoc-payment-methods{top:56px}}';
    	var is = '';
    	module.styles.push(styles, is);
    })();
    
    
    
    /************
     Module texts
     ************/
    
    var texts = JSON.parse('{"title":"Pago de Consulta"}');
    if(!module.texts) module.texts = {};
    $.extend(module.texts, texts);
    
    
    
    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<paper-toolbar>\r");t.b("\n" + i);t.b("    <paper-icon-button class=\"back\" icon=\"arrow-back\"></paper-icon-button>\r");t.b("\n" + i);t.b("    <span class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</span>\r");t.b("\n" + i);t.b("    <paper-spinner></paper-spinner>\r");t.b("\n" + i);t.b("</paper-toolbar>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<myddoc-payment-methods/>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /*********
    toolbar.js
    *********/
    
    function Toolbar($container) {
        "use strict";
    
        var back = $container.find('paper-toolbar .back').get(0);
        var spinner = $container.find('paper-toolbar paper-spinner').get(0);
        var control = $container.find('myddoc-payment-methods').get(0);
    
        back.addEventListener('click', beyond.back);
        console.log(control);
        function update() {
            console.log("updating");
            spinner.active = (control.fetching);
        }
    
        control.addEventListener('fetching-changed', update);
    
    }
    
    
    /******
    page.js
    ******/
    
    function Page($container, vdir) {
        "use strict";
    
        var toolbar;
        var control;
    
        this.preview = function () {
    
            $container.attr('id', 'payment-methods-page');
            var html = module.render('page', module.texts);
            $container.html(html);
    
            control = $container.find('myddoc-payment-methods').get(0);
            control.graphId = vdir;
    
    
            toolbar = new Toolbar($container);
    
        };
    
    
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/mercadopago/methods/page', 'code');

})(beyond.modules.get('libraries/myddoc/ui/mercadopago/methods/page'));