function Actions() {
    "use strict";

    var router = new Router();

    router.check = function (params, callback) {

        if (typeof Checkout === 'undefined') {
            callback(undefined, 'Checkout library error or not loaded.');
        }
        else {
            callback();
        }

    };

    router.createToken = function (params, callback) {

        var $form = $('<form method="post" />');

        function appendInput(dataCheckout, source) {
            var $input = $('<input type="text" />')
                .val(source)
                .attr('id', dataCheckout)
                .attr('placeholder', source)
                .attr('data-checkout', dataCheckout);
            $form.append($input);
        }

        appendInput('email', 'ebox@socites.com');
        appendInput('cardNumber', params.cardNumber);
        appendInput('securityCode', params.securityCode);
        appendInput('cardExpirationMonth', params.cardExpirationMonth);
        appendInput('cardExpirationYear', params.cardExpirationYear);
        appendInput('cardholderName', params.cardholderName);
        appendInput('docNumber', params.docNumber);
        appendInput('docType', params.docType);

        Checkout.createToken($form, function (status, response) {
            callback({
                'status': status,
                'response': response
            });
        });

    };

    router.getPaymentMethod = function (params, callback) {

        Checkout.getPaymentMethod(params.cardID, function (status, response) {
            callback({
                'status': status,
                'response': response
            });
        });

    };

    router.initialise();

}
