function Router() {

    var origin;
    var self = this;

    function postMessage(id, response, error) {

        var message = {
            'id': id,
            'response': response,
            'error': error
        };
        parent.postMessage(JSON.stringify(message), origin);

    }

    function onMessage(event) {

        origin = event.origin;
        var message = JSON.parse(event.data);

        var whiteList = ['check', 'destroy', 'createToken', 'getPaymentMethod'];
        if (whiteList.indexOf(message.action) !== -1) {

            if (!self[message.action]) {
                console.error('action "' + message.action + '" is not defined');
                return;
            }

            self[message.action](message.params, function (response, error) {
                postMessage(message.id, response, error);
            });

        }

    }

    this.destroy = function (params, callback) {
        window.removeEventListener('message', onMessage);
        callback();
    };

    this.initialise = function () {
        window.addEventListener('message', onMessage);
    };

}
