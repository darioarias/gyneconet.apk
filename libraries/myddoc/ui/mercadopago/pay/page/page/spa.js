/******************************
LIBRARY NAME: myddoc
MODULE: ui/mercadopago/pay/page
*******************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = undefined;

    /**********
     CSS STYLES
     **********/
    
    (function() {
    	var styles = '#mercadopago-pay-page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:#fff;opacity:0;transform:scale(.9) translate3d(0,0,0);transition:transform .4s ease,opacity .4s ease}#mercadopago-pay-page.show{opacity:1;transform:none}#mercadopago-pay-page paper-toolbar .title{font-size:16px;margin-left:0}#mercadopago-pay-page mercadopago-pay{position:absolute;top:64px;left:0;right:0;bottom:0;padding:16px;overflow-y:auto;-webkit-overflow-scrolling:touch}@media (max-width:600px){#mercadopago-pay-page mercadopago-pay{top:56px}}';
    	var is = '';
    	module.styles.push(styles, is);
    })();
    
    
    
    /************
     Module texts
     ************/
    
    var texts = JSON.parse('{"title":"Pago de Consulta"}');
    if(!module.texts) module.texts = {};
    $.extend(module.texts, texts);
    
    
    
    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<paper-toolbar>\r");t.b("\n" + i);t.b("    <paper-icon-button class=\"back\" icon=\"arrow-back\"></paper-icon-button>\r");t.b("\n" + i);t.b("    <span class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</span>\r");t.b("\n" + i);t.b("    <paper-spinner></paper-spinner>\r");t.b("\n" + i);t.b("</paper-toolbar>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<mercadopago-pay/>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /*********
    toolbar.js
    *********/
    
    function Toolbar($container) {
        "use strict";
    
        var back = $container.find('paper-toolbar .back').get(0);
        var spinner = $container.find('paper-toolbar paper-spinner').get(0);
        var control = $container.find('mercadopago-pay').get(0);
    
        back.addEventListener('click', beyond.back);
    
        function update() {
            spinner.active = (control.fetching || control.paying);
        }
    
        control.addEventListener('fetching-changed', update);
        control.addEventListener('paying-changed', update);
    
    }
    
    
    /******
    page.js
    ******/
    
    function Page($container) {
        "use strict";
    
        var toolbar;
        var control;
    
        this.preview = function () {
    
            $container.attr('id', 'mercadopago-pay-page');
            var html = module.render('page', module.texts);
            $container.html(html);
    
            control = $container.find('mercadopago-pay').get(0);
    
            toolbar = new Toolbar($container);
    
        };
    
        this.show = function () {
    
            var userId = this.querystring.userId;
            var inquiryType = this.querystring.inquiryType;
    
            control.specs = {'userId': userId, 'inquiryType': inquiryType};
    
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/mercadopago/pay/page', 'code');

})(beyond.modules.get('libraries/myddoc/ui/mercadopago/pay/page'));