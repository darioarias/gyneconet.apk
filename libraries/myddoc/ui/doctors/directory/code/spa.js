/***************************
LIBRARY NAME: myddoc
MODULE: ui/doctors/directory
****************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var react = module.react.items;

    var dependencies = module.dependencies;
    module.dependencies.set({"require":{"libraries/graphs/main/code":"graphs"}});

    /********
    loader.js
    ********/
    
    function DoctorsDirectory() {
        "use strict";
    
        var events = new Events({'bind': this});
    
        var collection;
        Object.defineProperty(this, 'collection', {
            'get': function () {
                return collection;
            }
        });
    
        var ready;
        Object.defineProperty(this, 'ready', {
            'get': function () {
                return !!ready;
            }
        });
    
        Object.defineProperty(this, 'loadedItemsCount', {
            'get': function () {
    
            }
        });
        Object.defineProperty(this, 'fullyLoaded', {
            'get': function () {
    
            }
        });
    
        dependencies.done(function (dependencies) {
    
            var graphs = dependencies.graphs;
            collection = new graphs.Graphs({
                'entitiesIds': '2',
                'limit': 5,
                'language': beyond.params.language,
                'extendedBy': 'doctor',
                'extAuthorized': '1'
            });
    
            ready = true;
            events.trigger('change');
    
            collection.load({'items': {'extensions': {'doctor': true}}})
                .then(function () {
                    console.log('loaded');
                });
    
            console.log(collection);
    
        });
    
    }
    
    
    /********
    define.js
    ********/
    
    define(function () {
        "use strict";
    
        return new DoctorsDirectory;
    
    });
    
    
    
    done('libraries/myddoc/ui/doctors/directory', 'code');

})(beyond.modules.get('libraries/myddoc/ui/doctors/directory'));