/***************************
LIBRARY NAME: myddoc
MODULE: ui/doctors/directory
****************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = undefined;

    /******
    page.js
    ******/
    
    function Page($container, vdir, dependencies) {
        "use strict";
    
        console.log('page', dependencies);
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/doctors/directory', 'code');

})(beyond.modules.get('libraries/myddoc/ui/doctors/directory'));