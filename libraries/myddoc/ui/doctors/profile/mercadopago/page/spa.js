/*************************************
LIBRARY NAME: myddoc
MODULE: ui/doctors/profile/mercadopago
**************************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = undefined;

    /**********
     CSS STYLES
     **********/
    
    (function() {
    	var styles = '#doctor-mp-token{position:absolute;top:0;right:0;bottom:0;left:0;background-color:#fff;opacity:0;transform:scale(.9) translate3d(0,0,0);transition:transform .4s ease,opacity .4s ease}#doctor-mp-token.show{opacity:1;transform:none}#doctor-mp-token paper-toolbar .title{font-size:16px;margin-left:0}#doctor-mp-token .content{position:absolute;top:64px;left:0;right:0;bottom:0;padding:16px;overflow-y:auto}#doctor-mp-token .content .processed{display:none}#doctor-mp-token .content .error-message{color:red;background:#ff0;padding:10px;display:none}#doctor-mp-token .content .continue{display:none;opacity:0;transition:transform .4s ease,opacity .4s ease}#doctor-mp-token .content .continue paper-button{margin:10px 0}#doctor-mp-token .content.ready .processing{display:none}#doctor-mp-token .content.ready.error .error-message,#doctor-mp-token .content.ready:not(.error) .processed{display:block}#doctor-mp-token .content.ready .continue{opacity:1;display:block}@media (max-width:600px){#doctor-mp-token .content{top:56px}}';
    	var is = '';
    	module.styles.push(styles, is);
    })();
    
    
    
    /************
     Module texts
     ************/
    
    var texts = JSON.parse('{"title":"Consultorio Digital"}');
    if(!module.texts) module.texts = {};
    $.extend(module.texts, texts);
    
    
    
    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<paper-toolbar>\r");t.b("\n" + i);t.b("    <span class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</span>\r");t.b("\n" + i);t.b("    <paper-spinner active></paper-spinner>\r");t.b("\n" + i);t.b("</paper-toolbar>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<div class=\"content\">\r");t.b("\n" + i);t.b("    <div class=\"processing\">Configurando cuenta MercadoPago ...</div>\r");t.b("\n" + i);t.b("    <div class=\"processed\">Su cuenta de MercadoPago ha sido vinculada con éxito.</div>\r");t.b("\n" + i);t.b("    <div class=\"error-message\">Error vinculando cuenta de MercadoPago, por favor vuelva a intentarlo nuevamente más\r");t.b("\n" + i);t.b("        tarde.\r");t.b("\n" + i);t.b("    </div>\r");t.b("\n" + i);t.b("    <div class=\"continue\">\r");t.b("\n" + i);t.b("        <paper-button raised>Continuar</paper-button>\r");t.b("\n" + i);t.b("    </div>\r");t.b("\n" + i);t.b("</div>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /*********
    js\page.js
    *********/
    
    function Page($container, vdir, dependencies) {
        "use strict";
    
        this.preview = function () {
    
            $container.attr('id', 'doctor-mp-token');
            var html = module.render('page', module.texts);
            $container.html(html);
    
            var spinner = $container.find('paper-spinner').get(0);
            var content = $container.find('.content');
            var close = $container.find('.continue paper-button');
    
            var graphs = dependencies.graphs;
    
            var qs = this.querystring;
    
            var closeFlag = qs.closeFlag;
    
            var origin = beyond.params.host;
            var redirect =
                origin +
                '/doctor/mp/token' +
                '?doctor_id=' + qs.doctor_id +
                '&mp_access_token=' + qs.mp_access_token +
                '&closeFlag=' + closeFlag;
    
            redirect = encodeURIComponent(redirect);
    
            close.click(function () {
                localStorage.setItem(closeFlag, true);
                window.close();
            });
    
            function done(error) {
                spinner.active = false;
                content.addClass('ready');
                if (error) {
                    content.addClass('error');
                }
            }
    
            graphs.payments.mp.tokens.set(qs.doctor_id, qs.code, qs.mp_access_token, redirect).promise
                .then(function () {
                    done(false);
                })
                .catch(function () {
                    done(true);
                });
    
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/doctors/profile/mercadopago', 'code');

})(beyond.modules.get('libraries/myddoc/ui/doctors/profile/mercadopago'));