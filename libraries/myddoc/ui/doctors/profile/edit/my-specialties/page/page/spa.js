/**************************************************
LIBRARY NAME: myddoc
MODULE: ui/doctors/profile/edit/my-specialties/page
***************************************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = "application/custom/myddoc/ui/doctors/profile/edit/my-specialties/page";

    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<paper-toolbar>\r");t.b("\n" + i);t.b("    <paper-icon-button class=\"back\" icon=\"graphs:arrow-back\"></paper-icon-button>\r");t.b("\n" + i);t.b("    <span class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</span>\r");t.b("\n" + i);t.b("    <paper-spinner></paper-spinner>\r");t.b("\n" + i);t.b("</paper-toolbar>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<myddoc-specialty-select/>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /*********
    toolbar.js
    *********/
    
    function Toolbar($container, control) {
        "use strict";
    
        var back = $container.find('paper-toolbar .back').get(0);
        var spinner = $container.find('paper-toolbar paper-spinner').get(0);
    
        back.addEventListener('click', function () {
            control.back()
                .then(function (exit) {
                    if (!exit) {
                        return;
                    }
                    beyond.back();
                });
    
        });
    
        function update() {
            spinner.active = (control.fetching);
        }
    
        control.addEventListener('fetching-changed', update);
    
    }
    
    
    /******
    page.js
    ******/
    
    function Page($container) {
        "use strict";
    
        var toolbar, control;
    
        this.preview = function () {
    
            $container.attr('id', 'specialty-select');
            var html = module.render('page', module.texts);
            $container.html(html);
    
            control = $container.find('myddoc-specialty-select').get(0);
            toolbar = new Toolbar($container, control);
    
        };
    
        this.prepare = function (done) {
            if (!control.ready) {
                control.addEventListener('ready', function () {
                    control.done(done);
                });
            }
            else {
                control.done(done);
            }
        };
    
        this.show = function () {
            control.update();
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/doctors/profile/edit/my-specialties/page', 'code');

})(beyond.modules.get('libraries/myddoc/ui/doctors/profile/edit/my-specialties/page'));