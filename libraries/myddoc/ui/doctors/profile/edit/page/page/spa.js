/***********************************
LIBRARY NAME: myddoc
MODULE: ui/doctors/profile/edit/page
************************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = undefined;

    /**********
     CSS STYLES
     **********/
    
    (function() {
    	var styles = '#doctor-profile-page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:#fff;opacity:0;transform:scale(.9) translate3d(0,0,0);transition:transform .4s ease,opacity .4s ease}#doctor-profile-page paper-toolbar{box-shadow:0 5px 10px -5px #333;z-index:1}#doctor-profile-page paper-toolbar .title{font-size:16px;margin-left:0}#doctor-profile-page myddoc-doctor-profile{position:absolute;top:64px;left:0;right:0;bottom:0;overflow-y:auto;-webkit-overflow-scrolling:touch}@media (max-width:600px){#doctor-profile-page myddoc-doctor-profile{top:56px}}#doctor-profile-page.show{opacity:1;transform:none}';
    	var is = '';
    	module.styles.push(styles, is);
    })();
    
    
    
    /************
     Module texts
     ************/
    
    var texts = JSON.parse('{"title":"Editar Perfil"}');
    if(!module.texts) module.texts = {};
    $.extend(module.texts, texts);
    
    
    
    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<paper-toolbar>\r");t.b("\n" + i);t.b("    <paper-icon-button class=\"back\" icon=\"arrow-back\"></paper-icon-button>\r");t.b("\n" + i);t.b("    <span class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</span>\r");t.b("\n" + i);t.b("    <paper-spinner active></paper-spinner>\r");t.b("\n" + i);t.b("</paper-toolbar>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<myddoc-doctor-profile></myddoc-doctor-profile>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /*********
    toolbar.js
    *********/
    
    function Toolbar($container) {
        "use strict";
    
        var back = $container.find('paper-toolbar .back').get(0);
        var spinner = $container.find('paper-toolbar paper-spinner').get(0);
        var control = $container.find('myddoc-doctor-profile').get(0);
    
        back.addEventListener('click', function () {
    
            control.exit()
                .then(function (exit) {
                    if (!exit) {
                        return;
                    }
    
                    beyond.back();
                });
        });
    
        function update() {
            spinner.active = (control.fetching || control.publishing);
        }
    
        control.addEventListener('fetching-changed', update);
        control.addEventListener('publishing-changed', update);
    
    }
    
    
    /******
    page.js
    ******/
    
    function Page($container) {
        "use strict";
    
        var toolbar;
    
        this.preview = function () {
    
            $container.attr('id', 'doctor-profile-page');
            var html = module.render('page', module.texts);
            $container.html(html);
    
            toolbar = new Toolbar($container);
    
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/doctors/profile/edit/page', 'code');

})(beyond.modules.get('libraries/myddoc/ui/doctors/profile/edit/page'));