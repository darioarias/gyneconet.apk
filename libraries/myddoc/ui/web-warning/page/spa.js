/*********************
LIBRARY NAME: myddoc
MODULE: ui/web-warning
**********************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = "application/custom/myddoc/ui/web-warning";

    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<div class=\"logo\"></div>\r");t.b("\n" + i);t.b("<div class=\"content\">\r");t.b("\n" + i);t.b("    <div class=\"texts\">\r");t.b("\n" + i);t.b("        <div class=\"top-text\">\r");t.b("\n" + i);t.b("            <div class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("            <div class=\"description\">");t.b(t.t(t.f("description",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("        </div>\r");t.b("\n" + i);if(t.s(t.f("downloadNow",c,p,1),c,p,0,256,458,"{{ }}")){t.rs(c,p,function(c,p,t){t.b("        <div class=\"bottom-text\">\r");t.b("\n" + i);t.b("            <div class=\"title\"><span class=\"primary\">");t.b(t.t(t.f("title",c,p,0)));t.b("</span></div>\r");t.b("\n" + i);t.b("            <div class=\"description\">");t.b(t.t(t.f("description",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("        </div>\r");t.b("\n" + i);});c.pop();}t.b("    </div>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("    <div class=\"actions\">\r");t.b("\n" + i);t.b("        <div class=\"app-buttons\">\r");t.b("\n" + i);t.b("            <div class=\"android\">\r");t.b("\n" + i);t.b("                <paper-button secondary raised>\r");t.b("\n" + i);t.b("                </paper-button>\r");t.b("\n" + i);t.b("            </div>\r");t.b("\n" + i);t.b("            <div class=\"ios\">\r");t.b("\n" + i);t.b("                <paper-button secondary raised>\r");t.b("\n" + i);t.b("                </paper-button>\r");t.b("\n" + i);t.b("            </div>\r");t.b("\n" + i);t.b("        </div>\r");t.b("\n" + i);t.b("    </div>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);if(t.s(t.f("footer",c,p,1),c,p,0,867,1270,"{{ }}")){t.rs(c,p,function(c,p,t){t.b("    <div class=\"footer\">\r");t.b("\n" + i);t.b("        <div class=\"center\">\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("            <div class=\"col-xs-10\">\r");t.b("\n" + i);t.b("                <div class=\"text\">");t.b(t.t(t.f("line1",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("                <div class=\"text\">");t.b(t.t(t.f("line2",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("            </div>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("            <div class=\"col-xs-2\">\r");t.b("\n" + i);t.b("                <paper-icon-button class=\"back\" icon=\"arrow-back\"></paper-icon-button>\r");t.b("\n" + i);t.b("            </div>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("        </div>\r");t.b("\n" + i);t.b("    </div>\r");t.b("\n" + i);});c.pop();}t.b("</div>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<paper-toast class=\"fit-bottom toast-error\"></paper-toast>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /*********
    js\page.js
    *********/
    
    function Page($container, parameter, dependencies) {
        "use strict";
    
        this.preview = function () {
    
            var texts = module.texts;
            texts.application = beyond.params.application.name;
    
            $container
                .attr('id', 'web-warning-page')
                .html(module.render('page', texts));
    
            var next = this.querystring.next;
            $container.find('paper-icon-button.back').click(function () {
                beyond.backToHome = true;
                if (!next) {
                    beyond.back();
                }
                else {
                    beyond.navigate(next);
                }
            });
    
            window.asd = $container;
            $container.find('.android paper-button').on('click', function () {
                var market = beyond.params.markets.android;
                if (!market) {
                    return;
                }
                window.open(market);
            });
            $container.find('.ios paper-button').on('click', function () {
                var market = beyond.params.markets.ios;
                if (!market) {
                    return;
                }
                window.open(market);
            });
    
        };
    
        this.prepare = function (done) {
            var img = new Image();
            img.src = module.host + '/static/images/logo.png';
            img.onload = done;
        };
    
        this.render = function () {
            $container.find('.actions').addClass('show');
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/web-warning', 'code');

})(beyond.modules.get('libraries/myddoc/ui/web-warning'));