/*******************************
LIBRARY NAME: myddoc
MODULE: ui/specialties/list/page
********************************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = "application/custom/myddoc/ui/specialties/list/page";

    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<paper-toolbar>\r");t.b("\n" + i);t.b("    <paper-icon-button class=\"back\" icon=\"arrow-back\"/>\r");t.b("\n" + i);t.b("    <span class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</span>\r");t.b("\n" + i);t.b("    <paper-spinner></paper-spinner>\r");t.b("\n" + i);t.b("    <paper-icon-button class=\"refresh\" icon=\"refresh\"/>\r");t.b("\n" + i);t.b("</paper-toolbar>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<myddoc-specialties-list/>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /*********
    toolbar.js
    *********/
    
    function Toolbar($container) {
        "use strict";
    
        var back = $container.find('paper-toolbar .back').get(0);
        var spinner = $container.find('paper-toolbar paper-spinner').get(0);
        var refresh = $container.find('paper-toolbar paper-icon-button.refresh').get(0);
        var control = $container.find('myddoc-specialties-list').get(0);
    
        back.addEventListener('click', beyond.back);
    
        refresh.addEventListener('click', function () {
            control.refresh();
        });
    
        function update() {
            spinner.active =
                (control.fetching &&
                control.dataSource === DATA_SOURCE.CACHE);
    
            refresh.disabled = control.fetching;
        }
    
        control.addEventListener('fetching-changed', update);
        control.addEventListener('data-source-changed', update);
        update();
    
    }
    
    
    /******
    page.js
    ******/
    
    function Page($container) {
        "use strict";
    
        var toolbar;
    
        this.preview = function () {
    
            $container.attr('id', 'specialties-page');
            var html = module.render('page', module.texts);
            $container.html(html);
    
            toolbar = new Toolbar($container);
    
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/specialties/list/page', 'code');

})(beyond.modules.get('libraries/myddoc/ui/specialties/list/page'));