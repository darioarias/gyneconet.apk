/*******************
LIBRARY NAME: myddoc
MODULE: ui/content
********************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = "application/custom/myddoc/ui/content";

    /************
     Module texts
     ************/
    
    var texts = JSON.parse('{"title":"Publicación"}');
    if(!module.texts) module.texts = {};
    $.extend(module.texts, texts);
    
    
    
    /*********
    js\page.js
    *********/
    
    function Page($container, vdir, dependencies) {
        "use strict";
    
        dependencies.PageBase.call(this, {
            'module': module,
            '$container': $container,
            'vdir': vdir,
            'texts': module.texts,
            'control': 'myddoc-article-view',
            'state': this.state
        });
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('libraries/myddoc/ui/content', 'code');

})(beyond.modules.get('libraries/myddoc/ui/content'));