/*******************
LIBRARY NAME: myddoc
MODULE: directory
********************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var react = module.react.items;

    /*************
    specialties.js
    *************/
    
    function Specialties(graphs) {
        "use strict";
    
        var auth = graphs.auth;
        var events = new Events({'bind': this});
    
        var fetching;
        Object.defineProperty(this, 'fetching', {
            'get': function () {
                return fetching;
            }
        });
    
        var fetched;
        Object.defineProperty(this, 'fetched', {
            'get': function () {
                return fetched;
            }
        });
    
        var error;
        Object.defineProperty(this, 'error', {
            'get': function () {
                return error;
            }
        });
    
        var loaded;
        Object.defineProperty(this, 'loaded', {
            'get': function () {
                return loaded;
            }
        });
    
        var entries;
        Object.defineProperty(this, 'entries', {
            'get': function () {
                return entries;
            }
        });
    
        var promise;
    
        function load(specialty) {
    
            if (promise) {
                return promise;
            }
            if (!auth.valid) {
                return;
            }
    
            fetching = true;
            events.trigger('change');
    
            var applicationId = beyond.params.application.id;
    
            var params = {
                'accessToken': auth.accessToken,
                'applicationId': applicationId,
                'specialty': specialty
            };
    
    
            var specialties = localStorage.getItem('specialtiesList');
            if (specialties) {
    
                try {
                    specialties = JSON.parse(specialties);
                    if (specialties.length) {
                        specialties = specialties;
                        loaded = true;
                    }
                } catch (err) {
    
                }
    
            }
    
            var action = new module.Action('doctors/specialties', params);
    
            action.onResponse = function (response) {
    
                error = undefined;
                promise = undefined;
                fetched = true;
                fetching = false;
                if (response) {
                    localStorage.setItem('specialtiesList', JSON.stringify(response));
                }
                entries = response.entries;
    
                events.trigger('change');
    
            };
            action.onError = function (response) {
    
                promise = undefined;
                fetching = false;
    
                if (response.code === action.ERROR_CODE.CANCELED) {
                    events.trigger('change');
                    return;
                }
    
                error = response.data;
                events.trigger('change');
    
            };
            promise = action.execute({'promise': true, 'policy': action.POLICY.COMMUNICATION_ERRORS});
            return promise;
    
        }
    
        this.load = load;
        this.refresh = load;
    
    }
    
    
    /*********
    doctors.js
    *********/
    
    function Doctors(specialty, specs) {
        "use strict";
    
        if (!specs) {
            specs = {};
        }
        var limit = (specs.limit) ? specs.limit : 10;
        var graphs = module.graphs;
        var events = new Events({'bind': this});
        var auth = graphs.auth;
    
        var start;
        Object.defineProperty(this, 'start', {
            'get': function () {
                return start;
            }
        });
        if (specs.start) {
            start = specs.start;
        }
    
        var fetching;
        Object.defineProperty(this, 'fetching', {
            'get': function () {
                return fetching;
            }
        });
    
        var fetched;
        Object.defineProperty(this, 'fetched', {
            'get': function () {
                return fetched;
            }
        });
    
        var error;
        Object.defineProperty(this, 'error', {
            'get': function () {
                return error;
            }
        });
    
        var entries = [];
        Object.defineProperty(this, 'entries', {
            'get': function () {
                return entries;
            }
        });
    
        var promise;
    
        function load() {
    
            if (promise) {
                return promise;
            }
            if (!auth.valid) {
                return;
            }
    
            fetching = true;
            events.trigger('change');
    
            var applicationId = beyond.params.application.id;
    
            var params = {
                'accessToken': auth.accessToken,
                'applicationId': applicationId,
                'specialty': specialty,
                'limit': limit
            };
    
            if (start) {
                params.start = start;
            }
    
            var action = new module.Action('doctors/specialty', params);
    
            action.onResponse = function (response) {
    
                error = undefined;
                promise = undefined;
                fetched = true;
                fetching = false;
                start = response.next;
    
                for (var key in response.doctors) {
    
                    var doctor = response.doctors[key];
                    var output = Object.assign({'doctorId': key}, doctor);
                    entries[key] = output;
    
                }
    
                events.trigger('change');
    
            };
    
            action.onError = function (response) {
    
                promise = undefined;
                fetching = false;
    
                if (response.code === action.ERROR_CODE.CANCELED) {
                    events.trigger('change');
                    return;
                }
    
                error = response.data;
                events.trigger('change');
    
            };
    
            promise = action.execute({'promise': true, 'policy': action.POLICY.COMMUNICATION_ERRORS});
            return promise;
    
        }
    
        this.load = load;
        this.refresh = load;
    
    }
    
    
    /********
    define.js
    ********/
    
    define(['libraries/graphs/main/code/' + beyond.params.language], function (graphs) {
        "use strict";
    
        module.graphs = graphs;
    
        // Expose Specialties
        var specialties = new Specialties(graphs);
    
        var output = {
            'doctors': Doctors,
            'specialties': specialties
        };
    
        console.log("Myddoc is being exposed as window._m. \n Use it only from the development console.")
        window._m = output;
        return output;
    
    });
    
    
    
    done('libraries/myddoc/directory', 'code');

})(beyond.modules.get('libraries/myddoc/directory'));