/***********
MODULE: home
************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = undefined;

    /**********
     CSS STYLES
     **********/
    
    (function() {
    	var styles = '#home-page,#home-page paper-scroll-header-panel{top:0;right:0;bottom:0;left:0;position:absolute}.btn-block{width:100%!important;text-align:center!important}paper-toolbar a,paper-toolbar a:active,paper-toolbar a:focus,paper-toolbar a:hover,paper-toolbar a:visited{color:#fff}paper-button.btn-primary,paper-button.primary,paper-button[primary]{color:#c42e7b}paper-button.btn-primary[raised],paper-button.primary[raised],paper-button[primary][raised]{background-color:#c42e7b;color:#fff}paper-button.btn-secondary,paper-button.secondary,paper-button[secondary]{color:#fff}paper-button.btn-secondary[raised],paper-button.secondary[raised],paper-button[secondary][raised]{background-color:#550144;color:#fff}paper-button.btn-success,paper-button.success,paper-button[success]{color:#c5e1a5}paper-button.btn-success[raised],paper-button.success[raised],paper-button[success][raised]{background-color:#c5e1a5;color:#7ab23b}paper-button.btn-info,paper-button.info,paper-button[info]{color:#c42e7b}paper-button.btn-info[raised],paper-button.info[raised],paper-button[info][raised]{background-color:#c5e1a5;color:#7ab23b}paper-button.btn-warning,paper-button.warning,paper-button[warning]{color:#ffe082}paper-button.btn-warning[raised],paper-button.warning[raised],paper-button[warning][raised]{background-color:#ffe082;color:#b58800}paper-button.btn-error,paper-button.error,paper-button[error]{color:#e57373}paper-button.btn-error[raised],paper-button.error[raised],paper-button[error][raised]{background-color:#e57373;color:#761616}paper-button.btn-link,paper-button.link,paper-button[link]{text-decoration:underline}paper-button[raised]{color:#550144}paper-button[disabled]{color:#656565!important;background:#e4e4e4!important}paper-icon-button.btn-primary,paper-icon-button.primary,paper-icon-button[primary]{color:#c42e7b}paper-icon-button.btn-primary:hover,paper-icon-button.primary:hover,paper-icon-button[primary]:hover{background-color:#e6eaf1;background-color:rgba(93,130,200,.2);border-radius:50%}paper-icon-button.btn-secondary,paper-icon-button.secondary,paper-icon-button[secondary]{color:#550144}paper-icon-button.btn-secondary:hover,paper-icon-button.secondary:hover,paper-icon-button[secondary]:hover{background-color:#333;background-color:rgba(51,51,51,.2);border-radius:50%}paper-icon-button.btn-success,paper-icon-button.success,paper-icon-button[success]{color:#c5e1a5}paper-icon-button.btn-success:hover,paper-icon-button.success:hover,paper-icon-button[success]:hover{background-color:#c5e1a5;background-color:rgba(197,225,165,.2);border-radius:50%}paper-icon-button.btn-info,paper-icon-button.info,paper-icon-button[info]{color:#c42e7b}paper-icon-button.btn-info:hover,paper-icon-button.info:hover,paper-icon-button[info]:hover{background-color:#f0f3ed;background-color:rgba(196,46,123,.2);border-radius:50%}paper-icon-button.btn-warning,paper-icon-button.warning,paper-icon-button[warning]{color:#ffe082}paper-icon-button.btn-warning:hover,paper-icon-button.warning:hover,paper-icon-button[warning]:hover{background-color:#f6f3ea;background-color:rgba(255,224,130,.2);border-radius:50%}paper-icon-button.btn-error,paper-icon-button.error,paper-icon-button[error]{color:#e57373}paper-icon-button.btn-error:hover,paper-icon-button.error:hover,paper-icon-button[error]:hover{background-color:#f3e8e8;background-color:rgba(229,115,115,.2);border-radius:50%}paper-checkbox,paper-checkbox #checkmark.paper-checkbox,paper-checkbox::shadow #checkmark.paper-checkbox{border-color:#fff!important}paper-dialog{color:#222;background-color:#f5f5f5;font-family:Roboto,sans-serif;font-weight:400}paper-dialog h1,paper-dialog h2,paper-dialog h3,paper-dialog h4,paper-dialog h5,paper-dialog h6{overflow:visible;padding:0 1em;text-align:center}paper-fab{--text-primary-color:white;background-color:#c42e7b;color:#333;--paper-fab-keyboard-focus-background:#711b47}paper-fab.btn-primary,paper-fab.primary,paper-fab[primary]{background-color:#c42e7b;color:#333;--paper-fab-keyboard-focus-background:#711b47}paper-fab.btn-secondary,paper-fab.secondary,paper-fab[secondary]{background-color:#550144;color:#000;--paper-fab-keyboard-focus-background:#000000}paper-fab.btn-success,paper-fab.success,paper-fab[success]{background-color:#c5e1a5;color:#c5e1a5;--paper-fab-keyboard-focus-background:#87c145}paper-fab.btn-info,paper-fab.info,paper-fab[info]{background-color:#c5e1a5;color:#3b4432;--paper-fab-keyboard-focus-background:#87c145}paper-fab.btn-warning,paper-fab.warning,paper-fab[warning]{background-color:#ffe082;color:#ffe082;--paper-fab-keyboard-focus-background:#ffc002}paper-fab.btn-error,paper-fab.error,paper-fab[error]{background-color:#e57373;color:#e57373;--paper-fab-keyboard-focus-background:#b72222}paper-fab[disabled]{color:#656565!important;background:#e4e4e4!important}section.hero paper-menu,section[hero] paper-menu{--paper-menu-background-color:gold}paper-tabs{background-color:#c42e7b;color:#333;box-shadow:0 5px 10px -5px #333}#home-page{background-color:#fff;opacity:0;transform:scale(.9) translate3d(0,0,0);transition:transform .4s ease,opacity .4s ease}#home-page #headerContainer{z-index:1}#home-page.show{opacity:1;transform:none}#home-page .box-test{height:1000px}#home-page .page-header{z-index:200}#home-page .content{z-index:1}#home-page paper-scroll-header-panel paper-toolbar{z-index:200}#home-page paper-scroll-header-panel paper-toolbar .picture{width:33px;height:20px;background-image:url(#host.application#images/icon.png);background-repeat:no-repeat;margin-right:5px}#home-page paper-scroll-header-panel paper-toolbar .title{font-size:16px;margin-left:0}#home-page paper-scroll-header-panel paper-toolbar paper-icon-button{padding:4px;width:30px;height:30px}#home-page paper-scroll-header-panel paper-toolbar paper-icon-button:last-child{margin-right:15px}#home-page paper-scroll-header-panel paper-toolbar paper-icon-button iron-icon{color:#fff}#home-page paper-scroll-header-panel paper-toolbar paper-icon-button.settings{width:26px;height:26px;margin:4px 4px 4px 9px}#home-page paper-scroll-header-panel paper-tabs{height:56px;box-shadow:0 -35px 20px 31px #000!important;z-index:100}#home-page paper-scroll-header-panel paper-tabs paper-ripple{color:#c42e7b}#home-page paper-scroll-header-panel paper-tab{text-transform:uppercase;font-size:13px}#home-page paper-scroll-header-panel paper-tab .conversations-badge{position:absolute;min-width:14px;top:5px;text-align:center;line-height:11px;right:-moz-calc(50% - 20px);right:-o-calc(50% - 20px);right:-webkit-calc(50% - 20px);right:calc(50% - 20px);-webkit-border-radius:50px;-moz-border-radius:50px;border-radius:50px;-moz-background-clip:padding;-webkit-background-clip:padding-box;padding:2px;font-size:10px;background:#fff;color:#fa65aa}#home-page paper-scroll-header-panel paper-tab .conversations-badge:empty{display:none}#home-page paper-scroll-header-panel iron-pages{position:absolute;top:120px;width:100%;z-index:1;height:-moz-calc(100% - 120px);height:-o-calc(100% - 120px);height:-webkit-calc(100% - 120px);height:calc(100% - 120px)}#home-page paper-scroll-header-panel iron-pages .p-view{padding-top:30px}#home-page paper-scroll-header-panel iron-pages .p-view.p-header{padding-top:0}@media (max-width:600px){#home-page paper-scroll-header-panel iron-pages{top:110px;height:-moz-calc(100% - 110px);height:-o-calc(100% - 110px);height:-webkit-calc(100% - 110px);height:calc(100% - 110px)}}#home-page paper-scroll-header-panel iron-pages graphs-channel{float:left;width:100%}';
    	var is = '';
    	module.styles.push(styles, is);
    })();
    
    
    
    /************
     Module texts
     ************/
    
    var texts = JSON.parse('{"title":"Gyneconet"}');
    if(!module.texts) module.texts = {};
    $.extend(module.texts, texts);
    
    
    
    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<paper-scroll-header-panel\r");t.b("\n" + i);t.b("    condenses\r");t.b("\n" + i);t.b("    condensed-header-height=\"60px\">\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("    <div class=\"paper-header\">\r");t.b("\n" + i);t.b("        <paper-toolbar>\r");t.b("\n" + i);t.b("            <div class=\"picture\"/>\r");t.b("\n" + i);t.b("            <div class=\"title\">");t.b(t.v(t.f("title",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("            <paper-spinner></paper-spinner>\r");t.b("\n" + i);t.b("            <paper-icon-button class=\"search\" icon=\"graphs:search\"/>\r");t.b("\n" + i);t.b("            <paper-icon-button class=\"refresh\" icon=\"graphs:refresh\"/>\r");t.b("\n" + i);t.b("            <paper-icon-button class=\"settings\" icon=\"graphs:settings\"/>\r");t.b("\n" + i);t.b("        </paper-toolbar>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("        <paper-tabs class=\"paper-header\" selected=\"5\">\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("            <paper-tab>\r");t.b("\n" + i);t.b("                <iron-icon icon=\"graphs:document\"/>\r");t.b("\n" + i);t.b("            </paper-tab>\r");t.b("\n" + i);t.b("            <paper-tab>\r");t.b("\n" + i);t.b("                <iron-icon icon=\"myddoc:doctor\"/>\r");t.b("\n" + i);t.b("            </paper-tab>\r");t.b("\n" + i);t.b("            <paper-tab>\r");t.b("\n" + i);t.b("                <iron-icon icon=\"graphs:comment-bubble\"/>\r");t.b("\n" + i);t.b("                <div class=\"conversations-badge\"></div>\r");t.b("\n" + i);t.b("            </paper-tab>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("        </paper-tabs>\r");t.b("\n" + i);t.b("    </div>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("    <iron-pages class=\"content\" selected=\"0\">\r");t.b("\n" + i);t.b("        <graphs-channel\r");t.b("\n" + i);t.b("                language=\"");t.b(t.v(t.f("language",c,p,0)));t.b("\"\r");t.b("\n" + i);t.b("                container=\"posts\"\r");t.b("\n" + i);t.b("                show-header\r");t.b("\n" + i);t.b("                auto-initialise/>\r");t.b("\n" + i);t.b("        <myddoc-specialties-list/>\r");t.b("\n" + i);t.b("        <graphs-conversations auto-initialise/>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("    </iron-pages>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("</paper-scroll-header-panel>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("page", template);
    
    
    /**********
    controls.js
    **********/
    
    function Controls($container) {
        "use strict";
    
        // Set the controls that support refresh & fetching + dataSource properties
        // The index must be set as the tab index where the control is located
        this[0] = $container.find('myddoc-specialties-list').get(0);
        this[1] = $container.find('graphs-conversations').get(0);
        this[2] = $container.find('graphs-channel').get(0);
    
    }
    
    
    /*********
    toolbar.js
    *********/
    
    function Toolbar($container, tabs, controls) {
        "use strict";
    
        var spinner = $container.find('paper-toolbar paper-spinner').get(0);
        var refresh = $container.find('paper-toolbar paper-icon-button.refresh').get(0);
        var search = $container.find('paper-toolbar paper-icon-button.search').get(0);
        var settings = $container.find('paper-toolbar paper-icon-button.settings').get(0);
    
        refresh.addEventListener('click', function () {
            controls[tabs.selected].refresh();
        });
    
        search.addEventListener('click', function () {
            beyond.navigate('/search');
        });
    
        settings.addEventListener('click', function () {
            beyond.navigate('/settings');
        });
    
        function update() {
    
            var selected = tabs.selected;
            if (!controls[selected]) {
                refresh.hidden = true;
                spinner.active = false;
                return;
            }
    
            // Update spinner
            spinner.active =
                (controls[selected].fetching &&
                controls[selected].dataSource === DATA_SOURCE.CACHE);
    
            // Update refresh button
            refresh.hidden = false;
            refresh.disabled = controls[selected].fetching;
    
        }
    
        tabs.addEventListener('iron-select', function () {
            var control = controls[tabs.selected];
            if (control.nodeName === 'GRAPHS-CONVERSATIONS' && !control.fetching) {
                control.refresh();
            }
            update();
        });
    
        for (var index in controls) {
            controls[index].addEventListener('fetching-changed', update);
            controls[index].addEventListener('data-source-changed', update);
        }
    
        update();
    
    }
    
    
    /******
    page.js
    ******/
    
    function Page($container, vdir, dependencies) {
        "use strict";
    
        var views, panel, tabs, pages, controls;
        var graphs = dependencies.graphs;
        var auth = graphs.auth;
    
        this.preview = function () {
    
            $container.attr({'id': 'home-page'});
    
            var texts = module.texts.copy();
            texts.language = beyond.params.language;
            var page = module.render('page', texts);
    
            $container.html(page);
    
            tabs = $container.find('paper-tabs').get(0);
            views = $container.find('iron-pages').get(0);
    
            panel = document.querySelector('paper-scroll-header-panel');
            panel.condensedHeaderHeight = '56';
    
            tabs.addEventListener('iron-select', function () {
                views.select(tabs.selected);
            });
    
            // This is a hack as for some reason, the tab is not marked as selected
            // when the app is loaded for the first time
            tabs.selected = 0;
            setTimeout(function () {
                tabs.notifyResize();
                panel.notifyResize();
                tabs.selected = -1;
                tabs.selected = 0;
            }, 1000);
    
            controls = new Controls($container);
            new Toolbar($container, tabs, controls);
    
            graphs.inbox.badge.$container = $container.find('.conversations-badge');
    
        };
    
        this.prepare = function (done) {
    
            if (!auth.valid) {
                beyond.navigate('/');
                return;
            }
            done();
    
        };
    
        this.show = function () {
    
            setTimeout(function () {
                panel.notifyResize();
            }, 300);
    
        };
    
        this.resume = function () {
    
            var control = controls[tabs.selected];
            if (control && !control.fetching) {
                control.done(function () {
                    control.refresh();
                });
            }
    
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('application/home', 'code');

})(beyond.modules.get('application/home'));