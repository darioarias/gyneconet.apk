/**************
MODULE: welcome
***************/

(function (params) {

    var done = params[1];
    var module = params[0];
    var dependencies = module.dependencies.modules;
    var react = module.react.items;

    var custom = undefined;

    /**********
     CSS STYLES
     **********/
    
    (function() {
    	var styles = '#myddoc-welcome,#myddoc-welcome .slides .slide{transition:transform .4s ease,opacity .4s ease;position:absolute}.btn-block{width:100%!important;text-align:center!important}paper-toolbar a,paper-toolbar a:active,paper-toolbar a:focus,paper-toolbar a:hover,paper-toolbar a:visited{color:#fff}paper-button.btn-primary,paper-button.primary,paper-button[primary]{color:#c42e7b}paper-button.btn-primary[raised],paper-button.primary[raised],paper-button[primary][raised]{background-color:#c42e7b;color:#fff}paper-button.btn-secondary,paper-button.secondary,paper-button[secondary]{color:#fff}paper-button.btn-secondary[raised],paper-button.secondary[raised],paper-button[secondary][raised]{background-color:#550144;color:#fff}paper-button.btn-success,paper-button.success,paper-button[success]{color:#c5e1a5}paper-button.btn-success[raised],paper-button.success[raised],paper-button[success][raised]{background-color:#c5e1a5;color:#7ab23b}paper-button.btn-info,paper-button.info,paper-button[info]{color:#c42e7b}paper-button.btn-info[raised],paper-button.info[raised],paper-button[info][raised]{background-color:#c5e1a5;color:#7ab23b}paper-button.btn-warning,paper-button.warning,paper-button[warning]{color:#ffe082}paper-button.btn-warning[raised],paper-button.warning[raised],paper-button[warning][raised]{background-color:#ffe082;color:#b58800}paper-button.btn-error,paper-button.error,paper-button[error]{color:#e57373}paper-button.btn-error[raised],paper-button.error[raised],paper-button[error][raised]{background-color:#e57373;color:#761616}paper-button.btn-link,paper-button.link,paper-button[link]{text-decoration:underline}paper-button[raised]{color:#550144}paper-button[disabled]{color:#656565!important;background:#e4e4e4!important}paper-icon-button.btn-primary,paper-icon-button.primary,paper-icon-button[primary]{color:#c42e7b}paper-icon-button.btn-primary:hover,paper-icon-button.primary:hover,paper-icon-button[primary]:hover{background-color:#e6eaf1;background-color:rgba(93,130,200,.2);border-radius:50%}paper-icon-button.btn-secondary,paper-icon-button.secondary,paper-icon-button[secondary]{color:#550144}paper-icon-button.btn-secondary:hover,paper-icon-button.secondary:hover,paper-icon-button[secondary]:hover{background-color:#333;background-color:rgba(51,51,51,.2);border-radius:50%}paper-icon-button.btn-success,paper-icon-button.success,paper-icon-button[success]{color:#c5e1a5}paper-icon-button.btn-success:hover,paper-icon-button.success:hover,paper-icon-button[success]:hover{background-color:#c5e1a5;background-color:rgba(197,225,165,.2);border-radius:50%}paper-icon-button.btn-info,paper-icon-button.info,paper-icon-button[info]{color:#c42e7b}paper-icon-button.btn-info:hover,paper-icon-button.info:hover,paper-icon-button[info]:hover{background-color:#f0f3ed;background-color:rgba(196,46,123,.2);border-radius:50%}paper-icon-button.btn-warning,paper-icon-button.warning,paper-icon-button[warning]{color:#ffe082}paper-icon-button.btn-warning:hover,paper-icon-button.warning:hover,paper-icon-button[warning]:hover{background-color:#f6f3ea;background-color:rgba(255,224,130,.2);border-radius:50%}paper-icon-button.btn-error,paper-icon-button.error,paper-icon-button[error]{color:#e57373}paper-icon-button.btn-error:hover,paper-icon-button.error:hover,paper-icon-button[error]:hover{background-color:#f3e8e8;background-color:rgba(229,115,115,.2);border-radius:50%}paper-checkbox,paper-checkbox #checkmark.paper-checkbox,paper-checkbox::shadow #checkmark.paper-checkbox{border-color:#fff!important}paper-dialog{color:#222;background-color:#f5f5f5;font-family:Roboto,sans-serif;font-weight:400}paper-dialog h1,paper-dialog h2,paper-dialog h3,paper-dialog h4,paper-dialog h5,paper-dialog h6{overflow:visible;padding:0 1em;text-align:center}paper-fab{--text-primary-color:white;background-color:#c42e7b;color:#333;--paper-fab-keyboard-focus-background:#711b47}paper-fab.btn-primary,paper-fab.primary,paper-fab[primary]{background-color:#c42e7b;color:#333;--paper-fab-keyboard-focus-background:#711b47}paper-fab.btn-secondary,paper-fab.secondary,paper-fab[secondary]{background-color:#550144;color:#000;--paper-fab-keyboard-focus-background:#000000}paper-fab.btn-success,paper-fab.success,paper-fab[success]{background-color:#c5e1a5;color:#c5e1a5;--paper-fab-keyboard-focus-background:#87c145}paper-fab.btn-info,paper-fab.info,paper-fab[info]{background-color:#c5e1a5;color:#3b4432;--paper-fab-keyboard-focus-background:#87c145}paper-fab.btn-warning,paper-fab.warning,paper-fab[warning]{background-color:#ffe082;color:#ffe082;--paper-fab-keyboard-focus-background:#ffc002}paper-fab.btn-error,paper-fab.error,paper-fab[error]{background-color:#e57373;color:#e57373;--paper-fab-keyboard-focus-background:#b72222}paper-fab[disabled]{color:#656565!important;background:#e4e4e4!important}section.hero paper-menu,section[hero] paper-menu{--paper-menu-background-color:gold}paper-tabs{background-color:#c42e7b;color:#333;box-shadow:0 5px 10px -5px #333}#myddoc-welcome{top:0;right:0;bottom:0;left:0;font-family:Roboto;opacity:0;transform:scale(.95) translate3d(0,50px,0)}#myddoc-welcome.show{opacity:1;transform:none}#myddoc-welcome .slides{position:absolute;top:0;left:0;right:0;bottom:39px;background:url(#host.module#img/background.jpg) center center no-repeat;background-size:cover}#myddoc-welcome .slides .slide .header{width:280px;height:280px;background-size:auto 95%;background-position:center center;background-repeat:no-repeat;margin:30px auto}@media (max-height:620px){#myddoc-welcome .slides .slide .header{width:250px;height:250px}}@media (max-height:520px){#myddoc-welcome .slides .slide .header{width:190px;height:190px}}@media (max-height:500px){#myddoc-welcome .slides .slide .header{width:180px;height:180px}}#myddoc-welcome .slides .slide .description,#myddoc-welcome .slides .slide .title{width:280px;margin:0 auto;text-align:center;font-size:22px;font-weight:lighter;color:#222}@media (max-height:560px){#myddoc-welcome .slides .slide .description,#myddoc-welcome .slides .slide .title{font-size:22px}}@media (max-height:520px){#myddoc-welcome .slides .slide .description,#myddoc-welcome .slides .slide .title{font-size:20px}}#myddoc-welcome .slides .slide paper-button.next-button{height:auto;width:110px;margin-left:-moz-calc(50% - 55px);margin-left:-o-calc(50% - 55px);margin-left:-webkit-calc(50% - 55px);margin-left:calc(50% - 55px);margin-top:20px;background-color:#c42e7b;color:#fff}#myddoc-welcome .slides .slide:nth-child(1) .header{background-image:url(#host.module#img/icon01.png);z-index:4}#myddoc-welcome .slides .slide:nth-child(2) .header{background-image:url(#host.module#img/icon02.png);z-index:3}#myddoc-welcome .slides .slide:nth-child(3) .header{background-image:url(#host.module#img/icon03.png);z-index:2}#myddoc-welcome .slides .slide:nth-child(4) .header{background-image:url(#host.module#img/icon04.png);z-index:1}#myddoc-welcome .slides .slide.front{top:0;left:0;width:100%;bottom:0}#myddoc-welcome .slides .slide.back{opacity:0;transform:scale(.95) translate3d(0,50px,0)}#myddoc-welcome .slides .slide.out{transform:translate3d(-100%,0,0)}#myddoc-welcome .slides .slide.start{z-index:0}#myddoc-welcome .slides .slide.end{z-index:1}#myddoc-welcome .slides .slide.hidden{display:none}#myddoc-welcome .myddoc{position:absolute;float:left;width:100%;left:0;bottom:0;height:39px;color:#fff;background:#073763;line-height:20px;text-indent:10px;font-size:13px;font-weight:lighter}#myddoc-welcome .myddoc .center{padding:7px 0;width:190px;margin-left:-moz-calc(50% - 97px);margin-left:-o-calc(50% - 97px);margin-left:-webkit-calc(50% - 97px);margin-left:calc(50% - 97px);float:left}#myddoc-welcome .myddoc .center .text{width:100px;float:left;line-height:25px}#myddoc-welcome .myddoc .center .logo{float:left;width:80px;height:25px;background:url(#host.module#img/myddoc-logo.png) center center no-repeat;background-size:auto 100%}';
    	var is = '';
    	module.styles.push(styles, is);
    })();
    
    
    
    /************
     Module texts
     ************/
    
    var texts = JSON.parse('{"slides":[{"title":"Comunicate con un amplio","description":"staff de <strong> profesionales en <br/> Fertilidad y Ginecología</strong> "},{"title":"<strong>Comunicación en tiempo real</strong> ","description":"por texto y video."},{"title":"Envía fotos y estudios","description":""},{"title":"Accedé a información relevante","description":"sobre <strong> temas de salud <br/> ginecológica y reproductiva</strong>"}],"footer":"Una aplicación"}');
    if(!module.texts) module.texts = {};
    $.extend(module.texts, texts);
    
    
    
    /******************
     MUSTACHE TEMPLATES
     ******************/
    
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<div class=\"header\"></div>\r");t.b("\n" + i);t.b("<div class=\"title\">");t.b(t.t(t.f("title",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("<div class=\"description\">");t.b(t.t(t.f("description",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("\r");t.b("\n" + i);t.b("<paper-button class=\"next-button\" raised>Siguiente</paper-button>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("slide", template);
    template = new Hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<div class=\"slides\"></div>\r");t.b("\n" + i);t.b("<div class=\"myddoc\">\r");t.b("\n" + i);t.b("    <div class=\"center\">\r");t.b("\n" + i);t.b("        <div class=\"text\">");t.b(t.v(t.f("footer",c,p,0)));t.b("</div>\r");t.b("\n" + i);t.b("        <div class=\"logo\"></div>\r");t.b("\n" + i);t.b("    </div>\r");t.b("\n" + i);t.b("</div>\r");t.b("\n");return t.fl(); },partials: {}, subs: {  }});
    module.templates.register("welcome", template);
    
    
    /******
    page.js
    ******/
    
    function Page($container) {
    
        var $slide1, $slide2, $slide3, $slide4;
    
        this.prepare = function (done) {
    
            $container
                .attr('id', 'myddoc-welcome')
                .html(module.render('welcome', module.texts));
    
            var img = new Image();
            var src = beyond.hosts.application.js + 'welcome/static/img/background.jpg';
            img.src = src;
            img.onload = done;
    
            var textsSlides = module.texts.slides;
    
            // Append slide 1
            $slide1;
            $slide1 = $('<div />')
                .addClass('slide front end')
                .html(module.render('slide', textsSlides[0]));
            $container.find('.slides').append($slide1);
    
            // Append slide 2
            $slide2;
            $slide2 = $('<div />')
                .addClass('slide back start')
                .html(module.render('slide', textsSlides[1]));
            $container.find('.slides').append($slide2);
    
            // Append slide 3
            $slide3;
            $slide3 = $('<div />')
                .addClass('slide back start')
                .html(module.render('slide', textsSlides[2]));
            $container.find('.slides').append($slide3);
    
            // Append slide 4
            $slide4;
            $slide4 = $('<div />')
                .addClass('slide back start')
                .html(module.render('slide', textsSlides[3]));
            $container.find('.slides').append($slide4);
    
            $slide1.find('paper-button').get(0).addEventListener('click', function () {
    
                $slide1.addClass('out');
                $slide2.removeClass('back start');
                $slide2.addClass('front end');
    
            });
    
            $slide2.find('paper-button').get(0).addEventListener('click', function () {
    
                $slide2.addClass('out');
                $slide3.removeClass('back start');
                $slide3.addClass('front end');
    
            });
    
            $slide3.find('paper-button').get(0).addEventListener('click', function () {
    
                $slide3.addClass('out');
                $slide4.removeClass('back start');
                $slide4.addClass('front end');
    
            });
    
            $slide4.find('paper-button').get(0).addEventListener('click', function () {
                localStorage.setItem('welcome', true);
                beyond.navigate('/');
            });
    
        };
    
    }
    
    
    
    define([custom], function() {
        if(typeof Page !== "function") {
            console.warn("Module does not have a Page function");
            return;
        }
        return Page;
    });

    done('application/welcome', 'code');

})(beyond.modules.get('application/welcome'));